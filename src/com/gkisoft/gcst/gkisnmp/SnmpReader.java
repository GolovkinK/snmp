package com.gkisoft.gcst.gkisnmp;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.event.ResponseListener;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.*;
import org.snmp4j.transport.DefaultUdpTransportMapping;

/**
 *  Created by pc7 on 10.02.2017.
 *  It is GKI Channel Searching Tool
 *  Comment
 */
public class SnmpReader implements ResponseListener {
    private int i;
    private static String snmpReadCommunity = "public";
    private static String snmpWriteCommunity = "private";
    private static final int    SNMP_RETRIES   = 3;
    private static final long   SNMP_TIMEOUT   = 1000L;
    private static final int    BULK_SIZE      = 500; //максимальный размер массива ответов, но кроме того он ещё ограничен размерами UDP пакета!!!
    private static int operationType; //тип того, что надо сделать. 1 - опрсить все OIDы; 2 - GETNEXT; 3 - получить список каналов с IRD. А по умолчанию - опросить один OID, тот что задан.
    private static String address = "udp:172.16.120.240/161"; //это значение по умолчанию, можно менять в любом месте программы, т.к. не final
    private static String startOid = "1.3.6.1";
    private static String valueToWriteInSetOperation = null;
    private String previousOid = "aaa"; //для отслеживания ситуаций, когда GETNEXT возвращает свой собственный OID

    private Snmp snmp = null;
    private TransportMapping transport = null;

    private Set<Integer32> requests = new HashSet<>();

    public static List<PDU> outputList = new LinkedList<>(); // это будет список для итератора. В него будем записывать значения ТОЛЬКО ИТЕРАТОРОМ. или не только... всё равно работает
//    private ListIterator iterator = outputList.listIterator(); // этот итератор будет перебирать список, в который будут постоянно добавляться новые ответы из полученных ответов onResponse
    private static Map<String,OneIrdChannel> channelMap = new HashMap<>();

    public static void main(String[] args) {
        if (args.length > 0) {
            Pattern p = Pattern.compile("((25[0-5]|2[0-4]\\d|[01]?\\d\\d?)\\.){3}(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)"); //для выделения IP адреса из аргументов
            for (String s : args) {
                if ("scanall".equals(s)) {
                    System.out.println("Будет выполнено сканирование всех доступных OID'ов.");
                    operationType = 1;
                } else if ("getnext".equals(s)) {
                    System.out.println("Будет выполнено сканирование одного следующего OID'а (GETNEXT).");
                    operationType = 2;
                } else if ("getchannels".equals(s)) {
                    System.out.println("Будет получен список каналов с IRD 2962.");
                    operationType = 3;
                } else if (s.contains("set=")) {
                    valueToWriteInSetOperation = s.substring(4,s.length());
                    operationType = 4;
                    System.out.println("Передаётся команда SET, значение устанавливаемого параметра ["+valueToWriteInSetOperation+"]");
                } else if (s.contains("startoid=")) {
                    startOid = s.substring(9,s.length());
                    System.out.println("Стартовый OID: "+startOid);
                } else if (s.equals("get")) {
                    operationType = -1;
                } else {
                    //если нашли IP адрес, то сканируем его
                    Matcher m = p.matcher(s);
                    if (m.matches()) {
                        address = "udp:" + s + "/161";
                    }
                }
            }
        } else {
            System.out.println("Доступные параметры: ");
            System.out.println("getchannels - получение списка каналов с IRD 2962.");
            System.out.println("scanall - сканирование всех доступных OID'ов.");
            System.out.println("getnext - сканирование одного следующего OID'а (GETNEXT).");
            System.out.println("set= - установка значения. (Использовать совместно со startoid=)");
            System.out.println("Например set=1 10.67.217.21 startoid=1.3.6.1.4.1.20938.1.2.14.1.2.1.3.9");
            System.out.println("значит, установить значение 1 в указанный OID");
            System.out.println("IP адрес в формате: 127.0.0.1");
            System.out.println("Например: scanall 172.16.120.240");
            System.out.println("startoid= - с какого OID'а начать? (без пробела!!!)");
            System.out.println("Например: startoid=1.3.6.1.4.1.4466.1.2.10.1.1.1.2.1.3.1392508928");
            System.out.println("");
            System.out.println("Не было найдено параметров getchannels, scanall, getnext или IP адреса типа 192.168.10.33, будут использованы параметры по умолчанию.");
            System.out.println("Будет выполнен обычный (GET) запрос одного OID'а 1.3.6.1");
        }
        System.out.println("Адрес: "+address);
        SnmpReader snmpReader = new SnmpReader();
        try {
            try {
                snmpReader.start();
            } finally {
                snmpReader.stop();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        if (operationType == 3) {
            List<PDU> clearedOutpudList = new LinkedList<>();
            //если не конец списка, то добавляем
            for (PDU p : outputList) {
                if (!"endOfMibView".equals(p.get(0).getVariable().toString())) {
                    clearedOutpudList.add(p);
                }
            }
            outputList = clearedOutpudList;
            readChannelsFromRequests(clearedOutpudList);
            for (Map.Entry e : channelMap.entrySet()) {
                System.out.println(e.getValue());
            }
        }

    }

    @Override
    public void onResponse(ResponseEvent event) {
        Integer32 requestId = event.getRequest().getRequestID();
        PDU response = event.getResponse();

        if (response != null) {
            //если полученный OID равен предыдущему, то добавим 1 к предпоследнему значению, да продолжим
            if ( previousOid.equals( response.get(0).getOid().toString() ) ) {
                Target t = getTarget(address);
                try {
                    System.out.println( "Получен дубль OID'а "+response.get(0).getOid().toString()+", перепрыгиваем на следующий уровень+1, возможны пропуски OID'ов!!!" );
                    String dubleOID = response.get(0).getOid().toString();
                    if (dubleOID.contains(".")) {
                        //отбрасываем конец :)
                        dubleOID = dubleOID.substring(0,dubleOID.lastIndexOf('.'));
                        System.out.println("после отброса конца OID: "+dubleOID);
                        if (dubleOID.contains(".")) {
                            try {
                                long l = Long.parseLong( dubleOID.substring(dubleOID.lastIndexOf('.')+1,dubleOID.length()) ); //берем число после последней оставшейся точки
                                l++;
                                dubleOID = dubleOID.substring(0,dubleOID.lastIndexOf('.')+1)+l;
                                System.out.println("Посылаем запрос OID'а "+dubleOID);
                                send(t, new String[] {dubleOID});
                            } catch (NumberFormatException ex) {
                                System.out.println("Ошибка, не удалось получить число из конца остстка OID'а: "+dubleOID);
                                ex.printStackTrace();
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }

            i++;
            System.out.println(i+": "+response.get(0).getOid()+" = "+response.get(0).getVariable());

            outputList.add(response);
            previousOid = response.get(0).getOid().toString();

        } else {
            synchronized (requests) {
                if (requests.contains(requestId)) {
                    System.out.println("Timeout exceeded");
                }
            }
        }
        synchronized (requests) {
            requests.remove(requestId);
        }
    }

    private void send(Target target, String[] oids) throws IOException {
        PDU pdu = new PDU();
        for (String oid: oids) {
            if (operationType == 4) {
                //если SET
                pdu.add(new VariableBinding(new OID(oid),new Integer32(Integer.parseInt(valueToWriteInSetOperation))));
                pdu.setType(PDU.SET);
            } else {
                //если GEt или GETNEXT
                pdu.add(new VariableBinding(new OID(oid)));
            }
        }

        if (operationType != 1 || operationType != 2 || operationType != 3 || operationType != 4) pdu.setType(PDU.GET); //по-умолчанию, если тип операции не задан

        if ( operationType == 1 || operationType == 2 || operationType == 3 ) {
            pdu.setType(PDU.GETNEXT);
        }
        if (operationType == 4) {
            pdu.setType(PDU.SET);
        }

        if (operationType != 4) {
            pdu.setMaxRepetitions(BULK_SIZE);
        }
        pdu.setNonRepeaters(0);
        ResponseEvent event = snmp.send(pdu, target, null);
        synchronized (requests) {
            requests.add(pdu.getRequestID());
        }
        onResponse(event);
    }

    private Target getTarget(String address) {
        Address targetAddress = GenericAddress.parse(address);
        CommunityTarget target = new CommunityTarget();
        if (operationType != 4) {
            target.setCommunity(new OctetString(snmpReadCommunity));
        } else {
            target.setCommunity(new OctetString(snmpWriteCommunity));
        }
        target.setAddress(targetAddress);
        target.setRetries(SNMP_RETRIES);
        target.setTimeout(SNMP_TIMEOUT);
        target.setVersion(SnmpConstants.version2c);
        return target;
    }

    private void start() throws IOException {
        transport = new DefaultUdpTransportMapping();
        snmp = new Snmp(transport);
        transport.listen();
        Target t = getTarget(address);

        if (operationType == 1) {  //если тип того, что надо сделать 1 - опрсить все OIDы
            send(t, new String[] {startOid});
            while (!"endOfMibView".equals( outputList.get(outputList.size()-1).get(0).getVariable().toString() )) {
                if (outputList.get(outputList.size()-1).get(0).getOid().toString().length() < 4) {
                    System.out.println("Слишком маленькая длина OID'а осталась (<4 символов). Останавливаем перебор. Где-то ошибка...");
                    System.out.println("Наверно, в последнем элементе дерева OID'ов не было сообщения \"endOfMibView\".");
                    return;
                }
                send(t, new String[] {outputList.get(outputList.size()-1).get(0).getOid().toString()});
            }
        } else if (operationType == 3) { //если надо получить каналы с IRD
            //собираем номер сервиса, тип, имя провайдера, имя канала.
            startOid = "1.3.6.1.4.1.4466.1.13.1.100.10.72.1.1";
            send(t, new String[] {startOid});
            while (outputList.get(outputList.size()-1).get(0).getOid().toString().startsWith("1.3.6.1.4.1.4466.1.13.1.100.10.72.1")) {
                send(t, new String[] {outputList.get(outputList.size()-1).get(0).getOid().toString()});
            }
            //собираем номера потоков.
            startOid = "1.3.6.1.4.1.4466.1.13.1.100.1.2.2.1.1";
            send(t, new String[] {startOid});
            while (outputList.get(outputList.size()-1).get(0).getOid().toString().startsWith("1.3.6.1.4.1.4466.1.13.1.100.10.72.1")) {
                send(t, new String[] {outputList.get(outputList.size()-1).get(0).getOid().toString()});
            }
        } else { //если надо GETNEXT или SET //в оставшихся случаях
            send(t, new String[]{startOid});
        }
    }

    private void stop() throws IOException {
        try {
            if (transport != null) {
                transport.close();
                transport = null;
            }
        } finally {
            if (snmp != null) {
                snmp.close();
                snmp = null;
            }
        }
    }
    private static void readChannelsFromRequests (List<PDU> list) {
        OneIrdChannel oneIrdChannel = new OneIrdChannel();
        for (int j = 0; j < list.size(); j++) {
            //надо создать  мап из каналов со своими ид, а потом их заполнять
            if (list.get(j).get(0).getOid().toString().startsWith("1.3.6.1.4.1.4466.1.13.1.100.10.72.1.1.")) {
                oneIrdChannel = new OneIrdChannel();
                oneIrdChannel.setId(list.get(j).get(0).getOid().toString().substring(38));
                channelMap.put(oneIrdChannel.getId(),oneIrdChannel);
            }
            if (list.get(j).get(0).getOid().toString().startsWith("1.3.6.1.4.1.4466.1.13.1.100.10.72.1.3.")) {
                oneIrdChannel = null;
                oneIrdChannel = channelMap.get(list.get(j).get(0).getOid().toString().substring(38));
                oneIrdChannel.setProgramNumber(list.get(j).get(0).getVariable().toString());
                channelMap.put(channelMap.get(list.get(j).get(0).getOid().toString().substring(38)).getId(),oneIrdChannel);
            }
            if (list.get(j).get(0).getOid().toString().startsWith("1.3.6.1.4.1.4466.1.13.1.100.10.72.1.8.")) {
                oneIrdChannel = null;
                oneIrdChannel = channelMap.get(list.get(j).get(0).getOid().toString().substring(38));
                oneIrdChannel.setScStreamDescrServiceType(OneIrdChannel.ScStreamDescrServiceType.parseType(list.get(j).get(0).getVariable().toString()));
                channelMap.put(channelMap.get(list.get(j).get(0).getOid().toString().substring(38)).getId(),oneIrdChannel);
            }
            if (list.get(j).get(0).getOid().toString().startsWith("1.3.6.1.4.1.4466.1.13.1.100.10.72.1.9.")) {
                oneIrdChannel = null;
                oneIrdChannel = channelMap.get(list.get(j).get(0).getOid().toString().substring(38));
                oneIrdChannel.setScStreamServiceProviderName(decodeRusFromHex(list.get(j).get(0).getVariable().toString()));
                channelMap.put(channelMap.get(list.get(j).get(0).getOid().toString().substring(38)).getId(),oneIrdChannel);
            }
            if (list.get(j).get(0).getOid().toString().startsWith("1.3.6.1.4.1.4466.1.13.1.100.10.72.1.10.")) {
                oneIrdChannel = null;
                oneIrdChannel = channelMap.get(list.get(j).get(0).getOid().toString().substring(39));
                oneIrdChannel.setScStreamServiceName(decodeRusFromHex(list.get(j).get(0).getVariable().toString()));
                channelMap.put(channelMap.get(list.get(j).get(0).getOid().toString().substring(39)).getId(),oneIrdChannel);
            }
        }
    }

    /**
     * Если строка содержит что-то, кроме HEX-символов и ":", то возвращаем её же, иначе метод
     * переводит сторку только HEX-данных и ":" ВЕРОЯТНО в кодировке iso 8859-5 в русские буквы.
     * @param str Строка только из HEX-данных и ":" вида 07:b2:01:11:05:2e:37:00:2d:00:00
     * @return Исходная строка или строка из русских символов, взятых из UTF-8, со смещением +864, чтобы совпадало с кодировкий iso 8859-5
     */
    public static String decodeRusFromHex (String str) {

        if (str.matches("[0-9A-Fa-f:]+")) {//содержит толдько HEX и ":"
            //тогда в сторке только HEX-данные ВЕРОЯТНО в кодировке iso 8859-5 (русский) вида 07:b2:01:11:05:2e:37:00:2d:00:00
            StringBuilder twoHexChars = new StringBuilder();
            StringBuilder decodedString = new StringBuilder();
            //переберём все символы в строке
            for (int j = 0; j < str.length(); j++) {
                //каждый третий символ - это ":", если его нашли, то переводим 2HEX символа в рус.букву и запоминаем её
                if ((j+1)%3 == 0 ) {
                    int num = Integer.parseInt(twoHexChars.toString(), 16);
                    //если символ не английский, то смещать его
                    if (num > 128) {
                        decodedString.append((char)(num+864));
                    } else {
                        decodedString.append((char)(num));
                    }
                    twoHexChars = new StringBuilder();
                    continue;
                }
                twoHexChars.append(str.charAt(j));
            }
            //запоминаем последнюю букву, т.к. после неё нет ":"
            int num = Integer.parseInt(twoHexChars.toString(), 16);
            //если символ не английский, то смещать его
            if (num > 128) {
                decodedString.append((char)(num+864));
            } else {
                decodedString.append((char)(num));
            }
            return decodedString.toString();
        } else {
            //тогда это обычная строка на английском.
            return str;
        }
    }

    public static String getSnmpReadCommunity() {
        return snmpReadCommunity;
    }

    public static String getSnmpWriteCommunity() {
        return snmpWriteCommunity;
    }

    public static void setSnmpWriteCommunity(String snmpWriteCommunity) {
        SnmpReader.snmpWriteCommunity = snmpWriteCommunity;
    }

    public static void setSnmpReadCommunity(String snmpReadCommunity) {
        SnmpReader.snmpReadCommunity = snmpReadCommunity;
    }
}
