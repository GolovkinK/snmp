package com.gkisoft.gcst.gkisnmp.etl;

import com.gkisoft.gcst.gkisnmp.SnmpReader;


import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;

/**
 * Created by k.golovkin on 05.07.17.
 */
public class GuiEtl {
    private boolean isConfirmNeedToShow = true;
    private int currentRowMouseEnteredTo;
    private int currentColumnMouseEnteredTo;
    private JFrame frame = new JFrame("ETL Matrix GKI Threatening GUI v1.2");

    private int numberOfInputs = 32;
    private int numberOfOutputs = 16;
    private JToggleButton[][] mas = new JToggleButton[numberOfOutputs][numberOfInputs];
    private String ipAddress = "127.0.0.1";
    private String[] inputLabels;
    private String[] outputLabels;
    private String snmpReadCommunity = "public";
    private String snmpWriteCommunity = "private";
    private String timeZone = "GMT+3";
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy (EEEE)");

    private MouseListener buttonsListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            JToggleButton temp = (JToggleButton)e.getSource();
            System.out.println("Мышь нажата на кнопку "+temp.getText());
            int curRowIndex = -1 + Integer.parseInt(temp.getText().substring(0,temp.getText().indexOf('_')));
            int curColumnIndex = -1 + Integer.parseInt(temp.getText().substring(temp.getText().indexOf('_')+1,temp.getText().length()));

            //если нажали левую кнопку мыши
            if (e.getButton() == MouseEvent.BUTTON1) {
                int n = -1;
                if (isConfirmNeedToShow) {
                    n = confirmCommutation();
                    if (n == 2) {
                        isConfirmNeedToShow = false;
                    }
                }
                if (n == 0 || !isConfirmNeedToShow) {
                    Date dateNow = new Date();
                    mas[curRowIndex][curColumnIndex].setToolTipText(mas[curRowIndex][curColumnIndex].getToolTipText()+"<br>Включили/выключили в "+dateFormat.format(dateNow));

                    //если кнопка не была красной
                    if (!temp.getBackground().equals(Color.RED)) {
                        //делаем фон кнопки из ответа коммутатора красным
                        System.out.println("Посылаем задание скоммутировать"+(curRowIndex+1)+","+(curColumnIndex+1)+" в коммутатор.");
                        int b = sendValueToEtlBySnmpReader(curRowIndex+1,curColumnIndex+1); //включаем коммутатор
                        for (int i = 0; i < numberOfInputs; i++) {
                            if (i == curColumnIndex && b == curColumnIndex+1) {
                                mas[curRowIndex][i].setBackground(Color.RED);
                            } else {
                                //если это не текущая кнопка, которая пока нажата от отжимаем её
                                mas[curRowIndex][i].setBackground(Color.LIGHT_GRAY);
                            }
                        }
                    } else {
                        //а если была красной, то выключаем
                        System.out.println("Посылаем задание скоммутировать"+(curRowIndex+1)+","+0+" в коммутатор.");
                        sendValueToEtlBySnmpReader(curRowIndex+1,0); //посылаем команду ВЫключения коммутации
                        for (int i = 0; i < numberOfInputs; i++) {
                            mas[curRowIndex][i].setBackground(Color.DARK_GRAY);
                        }
                    }
                }
                this.mouseEntered(e);
            }

            //если нажали правую кнопку мыши
            if (e.getButton() == MouseEvent.BUTTON3) {
                System.out.println("Нажали правую кнопку мыши.");
                Date dateNow = new Date();
                mas[curRowIndex][curColumnIndex].setToolTipText(mas[curRowIndex][curColumnIndex].getToolTipText()+"<br>Нажали правую кнопку мыши в "+dateFormat.format(dateNow));
                System.out.println(Calendar.getInstance().getTimeZone());
                System.out.println(mas[curRowIndex][curColumnIndex].getToolTipText());
                mas[curRowIndex][curColumnIndex].createToolTip();

                SnmpReader.setSnmpReadCommunity(snmpReadCommunity);
                for (int i = 0; i < numberOfOutputs; i++) {
                    SnmpReader.main(new String[] {"get","startoid=1.3.6.1.4.1.20938.1.2.14.1.2.1.3."+(i+1),ipAddress});
                }
                System.out.println("Таблица коммутации, полученная с коммутатора:");
                for (int i = 0; i < SnmpReader.outputList.size(); i++) {
                    String s = SnmpReader.outputList.get(i).get(0).getOid().toString();
                    System.out.print(s.substring(s.lastIndexOf('.')+1, s.length())+ "=" +SnmpReader.outputList.get(i).get(0).getVariable().toString()+", ");
                    try {
                        int readedCol = Integer.parseInt(SnmpReader.outputList.get(i).get(0).getVariable().toString());
                        for (int j = 0; j < numberOfInputs; j++) {
                            if (j == readedCol-1) {
                                //если совпала с включенной
                                mas[i][j].setBackground(Color.RED);
                            } else {
                                //если не совпала с включенной
                                mas[i][j].setBackground(Color.LIGHT_GRAY);
                                if (readedCol == 0) {
                                    mas[i][j].setBackground(Color.DARK_GRAY);
                                }
                            }
                        }
                    } catch (NumberFormatException ex) {
                        //если в настройках больше выходов, чем есть на самом деле
                        System.out.println("Нету такого входа коммутатора: "+s.substring(s.lastIndexOf('.')+1, s.length())+" "+ex);
                        for (int j = 0; j < numberOfInputs; j++) {
                            mas[i][j].setBackground(Color.BLACK);
                        }
                    }

                }
                SnmpReader.outputList.clear();
            }
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            //при ввходе мышки в область любой кнопки отображаются нажатыми все кнопки до нее в строке и столбце
            JToggleButton temp = (JToggleButton)e.getSource();
            int curRow = -1 + Integer.parseInt(temp.getText().substring(0,temp.getText().indexOf('_')));
            int curCol = -1 + Integer.parseInt(temp.getText().substring(temp.getText().indexOf('_')+1,temp.getText().length()));
            currentRowMouseEnteredTo = curRow;
            currentColumnMouseEnteredTo = curCol;
            //посылаем оповещение для JLabel'ов
            movingCursorOnButtonsListener.mouseClicked(e);
            //перебираем все кнопки
            for (int ri = 0; ri < numberOfOutputs; ri++) {
                //кнопки
                Color darkRed = new Color(173, 3, 17);
                for (int coli = 0; coli < numberOfInputs; coli++) {
                    //если ячейка попала в выделенные
                    if ( (ri == curRow || coli == curCol) && (ri < curRow || coli < curCol) ) {
                        //если она не была красной, то просто отображаем нажатую
                        if (mas[ri][coli].getBackground() != Color.RED) {
                            //если она была тёмно-красной то такой её и рисуем в выделенных ячейках
                            if (mas[ri][coli].getBackground().equals(darkRed)) {
                                mas[ri][coli].setBackground(darkRed);
                                continue;
                            }
                            mas[ri][coli].setSelected(true);
                        } else {
                            //если она была красной, то делаем её темнее
                            mas[ri][coli].setBackground(darkRed);
                        }
                    } else {
                        //если не должна выделяться
                        mas[ri][coli].setSelected(false);
                        //если была тёмно-красная, то сделать красную
                        if (mas[ri][coli].getBackground().equals(darkRed)) {
                            mas[ri][coli].setBackground(Color.RED);
                        }
                    }
                }
            }
            temp.updateUI();
        }
    };

     /**
     * Сюда приходят события от mouseEntered от JToggleButtons как будто они  были mouseClicked,
     * Этот слушатель используется для обновления фона полей Jlabel ну и его можно использовать
     * для редактирования этих полей.
     * */
    private MouseListener movingCursorOnButtonsListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            //если событие пришло от кнопки, то подсветим фон подписей нужного столбца и строки.
            if (e.getSource().getClass().equals(JToggleButton.class)) {
                //у нас уже есть текущие значения номера кнопки
/*                Component c = frame.getComponent(0);
                JRootPane r = (JRootPane)c;
                JLayeredPane jLayeredPane = (JLayeredPane)r.getComponent(1); //внутри 1эл. jpanel
                JPanel jp1 = (JPanel)jLayeredPane.getComponent(0); //1 Container
                Container container = (Container)jp1.getComponent(0); // 9 компонентов: (lable00,lable01,lable02,lable10,lable11,PANEL_hor12,lable20,PANEL_vert21,PANEL_button22)
          //      for (Component cc:container.getComponents()) {
          //          System.out.println(cc);
          //      }
                JPanel jPanel = (JPanel)container.getComponent(5);
         //       System.out.println(jPanel.getName());
*/

                JPanel horPanelofLabels = (JPanel)RecurseSearchOfComponentlWithName.resultOfSearch((JComponent)frame.getComponent(0),"Horizontal panel of column_name labels");
                //так быстрее без рекурсии, но зависит от внутренней структуры объектов
  //              JPanel horPanelofLabels = (JPanel)(((Container)((JPanel)((JLayeredPane)(((JRootPane)frame.getComponent(0)).getComponent(1))).getComponent(0)).getComponent(0)).getComponent(5));
                for (int i = 0; i < horPanelofLabels.getComponentCount(); i++) {
                    if (i == currentColumnMouseEnteredTo) {
                        horPanelofLabels.getComponent(i).setBackground(Color.RED);
                    } else {
                        horPanelofLabels.getComponent(i).setBackground(Color.GREEN);
                    }
                }
                horPanelofLabels.updateUI();


                //так быстрее без рекурсии, но зависит от внутренней структуры объектов
   //             JPanel vertPanelofLabels = (JPanel)(((Container)((JPanel)((JLayeredPane)(((JRootPane)frame.getComponent(0)).getComponent(1))).getComponent(0)).getComponent(0)).getComponent(7));
                JPanel vertPanelofLabels = (JPanel)RecurseSearchOfComponentlWithName.resultOfSearch((JComponent)frame.getComponent(0),"Vertical panel of row's_name labels");
                for (int i = 0; i < vertPanelofLabels.getComponentCount(); i++) {
                    if (i == currentRowMouseEnteredTo) {
                        vertPanelofLabels.getComponent(i).setBackground(Color.RED);
                    } else {
                        vertPanelofLabels.getComponent(i).setBackground(Color.GREEN);
                    }
                }
                vertPanelofLabels.updateUI();
            } else {
                System.out.println("Мышь нажата в объекте "+e.getSource());
                System.out.println("Пока что это ни на что не влияет. Можно редактировать подписи к полям, например...");
            }
        }
    };

    /**
     * Метод посылает команду по протоколу snmp в коммутатор, и в ответе по сети получает скоммутированные номера строки и столбца.
     * @param rowNumber номер выхода коммутатора.
     * @param columnNumber номер входа коммутатора.
     * @return -1 - ответ не получен, 0 - ничего не скоммутировано, цифру - номер скоммутированного столбца.
     */
    private int sendValueToEtlBySnmpReader(int rowNumber, int columnNumber) {
        SnmpReader.main(new String[] {"set="+columnNumber,"startoid=1.3.6.1.4.1.20938.1.2.14.1.2.1.3."+rowNumber,ipAddress});
        SnmpReader.setSnmpWriteCommunity(snmpWriteCommunity);
        System.out.print("Ответ коммутатора:");
        if (SnmpReader.outputList.isEmpty()) {
            //если ничего не ответил коммутатор
            System.out.println(" нет ответа...");
            return -1;
        } else {
            //если что-то ответил
            String s = SnmpReader.outputList.get(0).get(0).getOid().toString();

            System.out.println(" строка "+s.substring(s.lastIndexOf('.')+1, s.length())+ " скоммутирована со столбцом " +SnmpReader.outputList.get(0).get(0).getVariable().toString()+".");
            //если ответ подходящий
            int res = -1;
            try {
                res = Integer.parseInt(SnmpReader.outputList.get(0).get(0).getVariable().toString());
            } catch (NumberFormatException ex) {
                System.out.println("Это не номер столбца "+ex);
            }
                SnmpReader.outputList.clear();
                return res;
        }
    }

    /**
     * Выводит окно подтверждения и возвращает нажатую кнопку.
     * @return 0 - да, 1 - нет, 2 - больше не показывать это окно
     */
    private int confirmCommutation() {
        String[] yesOptions = {"Да, конечно!","Почему бы и нет!","Да, начальник!","Коммутируй уже!","Да, но только один раз!","Да, скоммутировать!"};
        String[] noOptions = {"Нет, только не это!","Нет, я передумал...","Нет, не делай это!","Нет, я случайно сюда тыкнул.","Неа!","Ничего не менять."};
        String[] dontShowOptions = {"Да, и больше не показывать это сообщение.",
                                    "Скоммутируй, и больше не подтверждай!",
                                    "Да, и не хочу видеть это подтверждение!",
                                    "Да, и не отвлекай меня такими сообщениями!",
                                    "Сделай что просят и уймись с этими подтверждениями уже!",
                                    "Да, только мне надоело это сообщение подтверждения."};
        Random random = new Random();
        Object[] options = {yesOptions[random.nextInt(6)], noOptions[random.nextInt(6)],dontShowOptions[random.nextInt(6)]};
        JFrame questionFrame = new JFrame();
        return JOptionPane.showOptionDialog(questionFrame,
                "Хочешь скоммутировать?",
                "Подтверждение изменения коммутации",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[0]);
    }

    private void createPanelUI() {
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);


        //  Это основной контейнер окна, состоящий из 3 строк. 1 - кнопки-вкладки доступных коммутаторов, 2 - матрица с подписями, 3 - срока лога.
        Container frameContainer = new Container();
        frameContainer.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        frameContainer.setLayout(new GridBagLayout());
        GridBagConstraints frameContainerConstraints = new GridBagConstraints();

        frameContainerConstraints.fill = GridBagConstraints.NONE;
        frameContainerConstraints.anchor = GridBagConstraints.WEST;
        frameContainerConstraints.gridx = 0;
        frameContainerConstraints.gridy = 0;
        frameContainerConstraints.weightx = 1;
        frameContainerConstraints.weighty = 0;


        JPanel commutatorsNames = new JPanel(new GridLayout(1,4,0,0)); // панель для 4 коммутаторов.
        commutatorsNames.setName("Labels of commutator's name panel");
        commutatorsNames.setBorder(new EtchedBorder());
        commutatorsNames.add(new JLabel("com1"){{setBorder(new EtchedBorder());}});
        commutatorsNames.add(new JLabel("com2"));
        commutatorsNames.add(new JLabel("com3"));
        commutatorsNames.add(new JLabel("com4"));
        commutatorsNames.setMinimumSize(new Dimension(500,50));
        commutatorsNames.setPreferredSize(new Dimension(500,50));
        commutatorsNames.setMaximumSize(new Dimension(500,50));

 //       commutatorsNames.addMouseListener(commutatorsNamesMouseListener);
        frameContainer.add(commutatorsNames,frameContainerConstraints); //x0.y0 - 1 строка интерфейса - имена коммутаторов

        Container matrixAndLabelsContainer = new Container();

        matrixAndLabelsContainer.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        matrixAndLabelsContainer.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 0;
        matrixAndLabelsContainer.add(new JLabel(""),constraints); //x0.y0

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = 0;
        matrixAndLabelsContainer.add(new JLabel(""),constraints); //x1.y0

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 2;
        constraints.gridy = 0;
        JLabel l = new JLabel(""); //x2.y.0 гор картинки
        l.setHorizontalAlignment(SwingConstants.CENTER);
  //      l.setBorder(new EtchedBorder());
        matrixAndLabelsContainer.add(l,constraints);

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 1;
        matrixAndLabelsContainer.add(new JLabel(""),constraints); //x0.y1

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = 1;
        matrixAndLabelsContainer.add(new JLabel(""),constraints); //x1.y1

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 2;
        constraints.gridy = 1;
        JPanel temp = new JPanel(new GridLayout(1,numberOfInputs,0,0));
        temp.setName("Horizontal panel of column_name labels");
        temp.addMouseListener(movingCursorOnButtonsListener);
        for (int i = 0; i < numberOfInputs; i++) {
            if (inputLabels[i] != null) {
                l = new JLabel(inputLabels[i]);
            } else {
                l = new JLabel("вход "+(i+1));
            }
            l.setHorizontalAlignment(SwingConstants.CENTER);
            l.setOpaque(true);
            l.setBackground(Color.GREEN);
            l.setBorder(new EtchedBorder());
            temp.add(l);
        }
        matrixAndLabelsContainer.add(temp,constraints);

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 2;
        matrixAndLabelsContainer.add(new JLabel(""),constraints); //x0.y2 верт картинки

        constraints.fill = GridBagConstraints.VERTICAL;
        constraints.gridx = 1;
        constraints.gridy = 2;
        temp = new JPanel(new GridLayout(numberOfOutputs,1,0,0));
        temp.setName("Vertical panel of row's_name labels");
        temp.addMouseListener(movingCursorOnButtonsListener);
        for (int i = 0; i < numberOfOutputs; i++) {
            if (outputLabels[i] != null) {
                l = new JLabel(outputLabels[i]);
            } else {
                l = new JLabel("выход "+(i+1));
            }
            l.setHorizontalAlignment(SwingConstants.CENTER);
            l.setOpaque(true);
            l.setBackground(Color.GREEN);
            l.setBorder(new EtchedBorder());
            temp.add(l);
        }
        matrixAndLabelsContainer.add(temp,constraints);

        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridy = 2;
        constraints.gridx = 2;
        constraints.weightx = 1;
        constraints.weighty = 1;

        //моя матрица кнопок
        JPanel panel = new JPanel(new GridLayout(numberOfOutputs,numberOfInputs,0,0));
        panel.setName("Buttons panel");
        Date dateNow = new Date();

        for (int rowi = 0; rowi < numberOfOutputs; rowi++) {
            for (int columni = 0; columni < numberOfInputs; columni++) {
                mas[rowi][columni] = new JToggleButton((rowi+1)+"_"+(columni+1));
                mas[rowi][columni].setBackground(Color.LIGHT_GRAY);
                mas[rowi][columni].addMouseListener(buttonsListener);
                mas[rowi][columni].setToolTipText("<HTML>Программа запущена в "+dateFormat.format(dateNow));
                mas[rowi][columni].createToolTip();
                panel.add(mas[rowi][columni]);
            }
        }
        matrixAndLabelsContainer.add(panel,constraints);

        //формируем центральную часть интерфейса - 2 строку (матрица с подписями)
        frameContainerConstraints.fill = GridBagConstraints.BOTH;
        frameContainerConstraints.gridx = 0;
        frameContainerConstraints.gridy = 1;
        frameContainerConstraints.weightx = 1;
        frameContainerConstraints.weighty = 1;
        //       commutatorsNames.addMouseListener(commutatorsNamesMouseListener);
        frameContainer.add(matrixAndLabelsContainer,frameContainerConstraints); //x0.y1 - 2 строка интерфейса

        frameContainerConstraints.fill = GridBagConstraints.HORIZONTAL;
        frameContainerConstraints.gridx = 0;
        frameContainerConstraints.gridy = 2;
        frameContainerConstraints.weightx = 1;
        frameContainerConstraints.weighty = 0;
        frameContainer.add(new JLabel("Log panel."){{setBorder(new EtchedBorder());}},frameContainerConstraints); //x0.y2 - 2 строка интерфейса

        frame.add(frameContainer);
        frame.pack();
        frame.setBounds(128,128,1600,800);
        frame.setVisible(true);
    }

    public static void main(String[] args) {

        GuiEtl guiEtl32x16 = new GuiEtl();

        guiEtl32x16.dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+3"));

        Properties properties = new Properties();
        try {
            InputStreamReader inputStream = new InputStreamReader(new FileInputStream("GuiEtl_config.properties"),"CP1251");
       //     InputStreamReader reader = new InputStreamReader (new FileReader("GuiEtl_config.properties")));
            properties.load(inputStream);
            inputStream.close();

            if (properties.getProperty("numberOfInputs") != null)     {guiEtl32x16.numberOfInputs = Integer.valueOf(properties.getProperty("numberOfInputs"));}
            if (properties.getProperty("numberOfOutputs") != null)    {guiEtl32x16.numberOfOutputs = Integer.valueOf(properties.getProperty("numberOfOutputs"));}
            //теперь определились с размерами, создаём соответствующее число кнопок вместо дефолтного в начале.
            guiEtl32x16.mas = new JToggleButton[guiEtl32x16.numberOfOutputs][guiEtl32x16.numberOfInputs];
            if (properties.getProperty("ipAddress") != null)          {guiEtl32x16.ipAddress = properties.getProperty("ipAddress");}
            if (properties.getProperty("snmpReadCommunity") != null)  {guiEtl32x16.snmpReadCommunity = properties.getProperty("snmpReadCommunity");}
            if (properties.getProperty("snmpWriteCommunity") != null) {guiEtl32x16.snmpWriteCommunity = properties.getProperty("snmpWriteCommunity");}
            if (properties.getProperty("timeZone") != null)          {
                guiEtl32x16.timeZone = properties.getProperty("timeZone");
                guiEtl32x16.dateFormat.setTimeZone(TimeZone.getTimeZone(guiEtl32x16.timeZone));
            }
            SnmpReader.setSnmpReadCommunity(guiEtl32x16.snmpReadCommunity);

            guiEtl32x16.inputLabels = new String[guiEtl32x16.numberOfInputs];
            for (int i = 0; i < guiEtl32x16.numberOfInputs; i++) {
                SnmpReader.main(new String[] {"get","startoid=1.3.6.1.4.1.20938.1.2.14.1.1.1.2."+(i+1),guiEtl32x16.ipAddress});
            }
            for (int i = 0; i < guiEtl32x16.numberOfInputs; i++) {
                guiEtl32x16.inputLabels[i] = (properties.getProperty("in"+(i+1)) == null) ? "<HTML><p align=center>"+SnmpReader.outputList.get(i).get(0).getVariable().toString()+"</p></HTML>" : properties.getProperty("in"+(i+1));
     //           guiEtl32x16.inputLabels[i] = properties.getProperty("in"+(i+1));
            }
            SnmpReader.outputList.clear();

            guiEtl32x16.outputLabels = new String[guiEtl32x16.numberOfOutputs];
            for (int i = 0; i < guiEtl32x16.numberOfOutputs; i++) {
                SnmpReader.main(new String[] {"get","startoid=1.3.6.1.4.1.20938.1.2.14.1.2.1.2."+(i+1),guiEtl32x16.ipAddress});
            }
            for (int i = 0; i < guiEtl32x16.numberOfOutputs; i++) {
                guiEtl32x16.outputLabels[i] = (properties.getProperty("out"+(i+1)) == null) ? "<HTML><p align=center>"+SnmpReader.outputList.get(i).get(0).getVariable().toString()+"</p></HTML>" : properties.getProperty("out"+(i+1));
      //          guiEtl32x16.outputLabels[i] = properties.getProperty("out"+(i+1));
            }
            SnmpReader.outputList.clear();

            if ("0".equals(properties.getProperty("isConfirmNeedToShow"))) {
                guiEtl32x16.isConfirmNeedToShow = false;
            }
        } catch (IOException e) {
            guiEtl32x16.inputLabels = new String[guiEtl32x16.numberOfInputs];
            guiEtl32x16.outputLabels = new String[guiEtl32x16.numberOfOutputs];
            System.out.println("ОШИБКА: Файл параметров отсуствует!"+e);
        }

       guiEtl32x16.createPanelUI();

        //посылаем ПКМ, как-будто мы нажали в кнопке 1_1.
        System.out.println("Программно нажимаем ПКМ в кнопке 1_1, чтобы прочитать состояние коммутатора.");
        guiEtl32x16.buttonsListener.mouseClicked(new MouseEvent(guiEtl32x16.mas[0][0],MouseEvent.MOUSE_CLICKED,System.currentTimeMillis(), 0,5,5,1,false,MouseEvent.BUTTON3));

        //надо отделить UI от логики, а то если нет связи с коммутатором, пока не опросит, невозможно закрыть окно
        //подписи столбцов сделать вертикально или по группам -антенна -диапазон - поляризация
        //добавить путь, где лежит один общий конфиг для всех чтоб комментарии и блокировки делать
        //сделать, чтобы использовалась snp4j, а не SnmpReader
        // получать trap'ы от коммутатора или ещё чего...
        //из мэйна всё надо преренести в конструктор, чтоб управлять множеством коммутаторов

    }
}
