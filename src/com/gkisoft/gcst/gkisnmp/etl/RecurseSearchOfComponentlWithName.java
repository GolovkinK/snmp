package com.gkisoft.gcst.gkisnmp.etl;

import java.awt.*;

/**
 * Created by k.golovkin on 22.07.17.
 * Этот класс нужен для рекурсивного поиска контейнера с нужным названием
 * внутри кучи контейнеров, вложенных друг в друга.
 *
 * Один голый метод рекурсивного поиска возвращает всегда null, т.к. всегда последним проверяется не нужный компонент.
 * Поэтому, запоминаем, когда найдём нужный, да возвращаем его, когда рекурсивный метод пройдёт все элементы.
 * Не знаю, как вывести сразу рекурсивным методом искомое значение без отдельной внешней переменной result.
 */
public class RecurseSearchOfComponentlWithName {
    private static Container result = new Container();

    private static Container recurseSearchOfJPanelWithName (Container container, String nameOfJPanel) {
        if (nameOfJPanel.equals(container.getName())) {
            //если имеем нужный элемент, то запоминаем его.
 //           System.out.println("BINGO=============================================================================================================================================================");
            result = container;
            return container;
        } else {
            //если это не искомый элемент, то ищем нужное во всех вложенных элементах рекурсивно.
  //          System.out.println("Не бинго. контейнер "+container.getClass()+" "+container.getName());
            for (Component c:container.getComponents()) {
  //              System.out.println("Поиск имени в компоненте "+c.getClass()+" "+c.getName());
                recurseSearchOfJPanelWithName((Container) c,nameOfJPanel);
            }
            return container;
        }
    }

    /**
     * Этот метод возвращает найденный в результате рекурсивного поиска Container с нужным названием.
     * @param container - контейнер, содержащий нужный элемент
     * @param nameOfJPanel - имя искомого контейнера.
     * @return найденный контейнер с искомым именем в переданном контейнере.
     */
    public static Container resultOfSearch(Container container, String nameOfJPanel) {
        recurseSearchOfJPanelWithName (container, nameOfJPanel);
        return result;
    }
}
