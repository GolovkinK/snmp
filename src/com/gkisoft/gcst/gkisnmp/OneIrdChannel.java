package com.gkisoft.gcst.gkisnmp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by pc7 on 03.06.2017.
 */
public class OneIrdChannel {

    private String id;                                        //                                       вида 1178599680.9.1.0.0.1.0
    private String programNumber;                             //OID	 .1.3.6.1.4.1.4466.1.13.1.100.10.72.1.3, например, 405
    private ScStreamDescrServiceType scStreamDescrServiceType;//OID	 .1.3.6.1.4.1.4466.1.13.1.100.10.72.1.8 Тип сервиса, напр Digital television service
    private String scStreamServiceProviderName;               //OID	 .1.3.6.1.4.1.4466.1.13.1.100.10.72.1.9, напр. GAZPROM_SS
    private String scStreamServiceName;                       //OID	 .1.3.6.1.4.1.4466.1.13.1.100.10.72.1.10, напр. TV ZVEZDA

 //   String oid = "1.3.6.1.4.1.4466.1.13.1.100.10.72.1.1.1178599680.9.1.0.0.1.0";
 //   String data = "1178599680";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProgramNumber() {
        return programNumber;
    }

    public void setProgramNumber(String programNumber) {
        this.programNumber = programNumber;
    }

    public ScStreamDescrServiceType getScStreamDescrServiceType() {
        return scStreamDescrServiceType;
    }

    public void setScStreamDescrServiceType(ScStreamDescrServiceType serviceType) {
        this.scStreamDescrServiceType = serviceType;
    }

    public String getScStreamServiceProviderName() {
        return scStreamServiceProviderName;
    }

    public void setScStreamServiceProviderName(String providerName) {
        this.scStreamServiceProviderName = providerName;
    }

    public String getScStreamServiceName() {
        return scStreamServiceName;
    }

    public void setScStreamServiceName(String serviceName) {
        this.scStreamServiceName = serviceName;
    }

    @Override
    public String toString() {
                return String.format("Номер программы(SID) %5s, Тип сервиса %30s, Имя провайдера %15s, Название программы %s",
                        programNumber, scStreamDescrServiceType.type, scStreamServiceProviderName, scStreamServiceName);
    }

    public static void main(String[] args) throws IOException {
        List<String> oid = new ArrayList<>();
        List<String> dat = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader("e:\\snmp\\мое\\snmp\\all_OID_ird_01032017_8_3674.txt"));
        while (reader.ready()) {
            String s = reader.readLine();
            if (s.contains("1.3.6.") && s.contains("=")) {
                oid.add(s.substring(s.indexOf("1.3.6."),s.indexOf('=')-1));
                dat.add(s.substring(s.indexOf('=')+2));
            }
        }
        reader.close();
        readChannelsFromRequests(oid, dat);
    }

    /**
     * Тип сервиса.
     */
    public enum  ScStreamDescrServiceType {
        _0("Reserved for future use"),
        _1("Digital television service"),
        _2("Digital radio sound service"),
        _3("Teletext service"),
        _4("NVOD reference service"),
        _5("NVOD time shifted service"),
        _6("Mosaic service"),
        _7("PAL coded signal"),
        _8("SECAM coded signal"),
        _9("D D2 MAC"),
        _10("FM Radio"),
        _11("NTSC coded signal"),
        _12("Data broadcast service"),
        _UNKNOWN("Не определён");

        private String type;

        /**это контруктор, который создает нужный тип из списка, он всегда приватный,
         * доступен только внутри enum
         * */
        ScStreamDescrServiceType (String chType) {
            this.type = chType;
        }
        /** Этот метод определяет тип из переданной строки.*/
        public static ScStreamDescrServiceType parseType (String stringToParse){
            if ( "0".equals(stringToParse) ) {
                return _0;
            } else if ( "1".equals(stringToParse) ) {
                return _1;
            } else if ( "2".equals(stringToParse) ) {
                return _2;
            } else if ( "3".equals(stringToParse) ) {
                return _3;
            } else if ( "4".equals(stringToParse) ) {
                return _4;
            } else if ( "5".equals(stringToParse) ) {
                return _5;
            } else if ( "6".equals(stringToParse) ) {
                return _6;
            } else if ( "7".equals(stringToParse) ) {
                return _7;
            } else if ( "8".equals(stringToParse) ) {
                return _8;
            } else if ( "9".equals(stringToParse) ) {
                return _9;
            } else if ( "10".equals(stringToParse) ) {
                return _10;
            } else if ( "11".equals(stringToParse) ) {
                return _11;
            } else if ( "12".equals(stringToParse) ) {
                return _12;
            } else  {
                return _UNKNOWN;
            }
        }

    }

    /**
     * Читает и выводит на экран список программ из коллекций с OID'ами и данными, считанными откуда-то, например, из файла.
     * @param oid коллекция OID'ов.
     * @param dat колекция данных, соответствующих OID'ам.
     */
    private static void readChannelsFromRequests (List<String> oid, List<String> dat) {
        OneIrdChannel oneIrdChannel;
        Map<String,OneIrdChannel> channelMap = new HashMap<>();
        for (int j = 0; j < oid.size(); j++) {
            //надо создать  мап из каналов со своими ид, а потом их заполнять
            if (oid.get(j).startsWith("1.3.6.1.4.1.4466.1.13.1.100.10.72.1.1.")) {
                oneIrdChannel = new OneIrdChannel();
                oneIrdChannel.setId(oid.get(j).substring(38));
                channelMap.put(oid.get(j).substring(38),oneIrdChannel);
            }
            if (oid.get(j).startsWith("1.3.6.1.4.1.4466.1.13.1.100.10.72.1.3.")) {
                oneIrdChannel = null;
                oneIrdChannel = channelMap.get(oid.get(j).substring(38));
                oneIrdChannel.setProgramNumber(dat.get(j));
                channelMap.put(oid.get(j).substring(38),oneIrdChannel);
            }
            if (oid.get(j).startsWith("1.3.6.1.4.1.4466.1.13.1.100.10.72.1.8.")) {
                oneIrdChannel = null;
                oneIrdChannel = channelMap.get(oid.get(j).substring(38));
                oneIrdChannel.setScStreamDescrServiceType(OneIrdChannel.ScStreamDescrServiceType.parseType(dat.get(j)));
                channelMap.put(oid.get(j).substring(38),oneIrdChannel);
            }
            if (oid.get(j).startsWith("1.3.6.1.4.1.4466.1.13.1.100.10.72.1.9.")) {
                oneIrdChannel = null;
                oneIrdChannel = channelMap.get(oid.get(j).substring(38));
                oneIrdChannel.setScStreamServiceProviderName(SnmpReader.decodeRusFromHex(dat.get(j)));
                channelMap.put(oid.get(j).substring(38),oneIrdChannel);
            }
            if (oid.get(j).startsWith("1.3.6.1.4.1.4466.1.13.1.100.10.72.1.10.")) {
                if (channelMap.containsKey(oid.get(j).substring(39))) {
                    oneIrdChannel = null;
                    oneIrdChannel = channelMap.get(oid.get(j).substring(39));
                    oneIrdChannel.setScStreamServiceName(SnmpReader.decodeRusFromHex(dat.get(j)));
                    channelMap.put(oid.get(j).substring(39), oneIrdChannel);
                }
            }
        }
        for (Map.Entry e:channelMap.entrySet()) {
            System.out.println(String.format("%25s %s",e.getKey(),e.getValue()));
        }
    }
}
