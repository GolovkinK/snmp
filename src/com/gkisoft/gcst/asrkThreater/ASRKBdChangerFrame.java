package com.gkisoft.gcst.asrkThreater;

// Пример работы со стандартной моделью таблицы JTable

import javax.swing.*;
import javax.swing.table.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class ASRKBdChangerFrame extends JFrame
{
	private static final long serialVersionUID = 1L;
	private String[] columnsHeader = new String[] {"Номер","Перезаписать?","Дата протокола","Протокол","Отправлено в УРК","Номер задания","Дата задания","Владелец","Дом","Улица","Фонарь","Аптека","Трудозатраты","Примечание"};
	private ArrayList<ArrayList<String>> dataFromDB;
	private String[] urkIsReady; //для проверки, если стояла "1", то нельзя снимать флаг.
    private Map<String,Map<String,Integer>> kaOwnersMap = new HashMap<>(); // мапа, содержащая всех владельцев КА и их кол-во для каждого КА, чтоб потом искать лидера:)
    private JComboBox<String> comboBox;
    private JTextField loginField = new JTextField("gki67");
    private JPasswordField passwordField = new JPasswordField();
    // бдрк или бдрк-тест?
    private JComboBox<String> bdrkComboBox = new JComboBox<>(new String[]{"bdrk","bdrk_test"});
    private static Date startDate;
    private static JButton read;
    private static JButton fill;
    private static JButton write;
    // Создание таблицы на основе модели данных
    private JTable table = new JTable(new SimpleModel()){
        @Override
        public TableCellEditor getCellEditor(int row, int column) {
            String kaName = dataFromDB.get(row).get(10);
            // если есть КА в kaOwnersMap, который содержится в текущей строке и заполненном из АСРК столбце
            if (kaOwnersMap.containsKey(kaName) && column == 7) {
                int itemmsSize = kaOwnersMap.get(kaName).size(); // кол-во владельцев конкретного КА.
                ArrayList<String> items = new ArrayList<>(itemmsSize); // это конкретные владельцы с указанием их кол-ва конкретного КА.
                for ( Map.Entry entry:( kaOwnersMap.get(kaName).entrySet() ) ) {
                    items.add(entry.getKey() +" ["+entry.getValue()+" шт.]");
                }
                Collections.sort(items, new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        o1 = o1.substring(o1.lastIndexOf("[")+1,o1.lastIndexOf(" шт.]"));
                        o2 = o2.substring(o2.lastIndexOf("[")+1,o2.lastIndexOf(" шт.]"));
                        int int1 = 0;
                        int int2 = 0;
                        try {
                            int1 = Integer.parseInt(o1);
                            int2 = Integer.parseInt(o2);
                        } catch (NumberFormatException ex) {
                         //   ex.printStackTrace();
                            System.out.println(ex);
                        }
                        return int2 - int1;
                    }
                });
                String[] itemsStringArr = new String[items.size()];
                for (int i = 0; i < items.size(); i++) {
                    itemsStringArr[i] = items.get(i);
                }
                comboBox = new JComboBox<>(itemsStringArr);
                return new DefaultCellEditor(comboBox);
            } else {
                return super.getCellEditor(row, column);
            }
        }
    };

	public ASRKBdChangerFrame()
	{
		super("Заполнитель хрени. v 1.1");
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        // Создание надписи запроса
        JLabel queryLabel = new JLabel("Запрос в АСРК:");

		// Создание области фильтра в виде SQL запроса
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String nowDate = sdf.format(new Date());
        JTextField queryField = new JTextField("PROTOCOL_NUMBER LIKE '%39100%' AND PROTOCOL_DATE LIKE '"+nowDate+"%'");
        queryField.setPreferredSize(new Dimension(500,20));

        // Создание логина
        JLabel loginLabel = new JLabel("Логин в АСРК:");

        // Ввод логина
        loginField.setPreferredSize(new Dimension(50,20));

        // Создание логина
        JLabel passwordLabel = new JLabel("Пароль в АСРК:");

        // Ввод пароля
        passwordField.setPreferredSize(new Dimension(50,20));

        JLabel bdrkLabel = new JLabel("База данных АСРК:");

		// Создание кнопки чтения
        read = new JButton("<html><font color=red>Прочитать</font> из АСРК, что там записано</html>");
		read.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    readFromDB(queryField.getText(),loginField.getText(),new String(passwordField.getPassword()),bdrkComboBox.getSelectedItem().toString());
			    table.updateUI();
			}
		});
		// Создание кнопки заполнения
        fill = new JButton("<html><font color=red>Заполнить</font> все выделенные строки в столбце 2 тем, чем надо</html>");
		fill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
                System.out.println("Заполняем все выбранные столбцы тем, что должно в них быть...");

                // надо разобраться с владельцами
                // делать запрос в БД, изменять тип столбца, делать выпадающий список с победителем по умолчанию
                try {
                    Class.forName("org.firebirdsql.jdbc.FBDriver");
                    Connection connection = DriverManager.getConnection("jdbc:firebirdsql:172.16.120.209/3050:"+bdrkComboBox.getSelectedItem().toString()+"?encoding=win1251",loginField.getText(),new String(passwordField.getPassword()));
                    Statement statement = connection.createStatement();
                    // только спутниковые протоколы
                    ResultSet rs1 = statement.executeQuery("SELECT * FROM RK_WORK_CONTROL_LOCAL WHERE PROTOCOL_NUMBER LIKE '%39100%'");
                    kaOwnersMap = new HashMap<>(); // если не обнулить, то при каждом нажатии кнопки владельцы плюсуются.
                    // данные таблиц
                    while (rs1.next()) {
                        if ( kaOwnersMap.containsKey(rs1.getString(19)) ) {
                            // если RES_NAS_PUNKT (19 столбец == назв. КА) содержится в мапе
                            // достаём мапу владельцев
                            Map<String,Integer> ownerNumberMap = kaOwnersMap.get(rs1.getString(19));
                            if ( ownerNumberMap.containsKey(rs1.getString(16)) && !"НЕИЗВЕСТЕН".equals(rs1.getString(16)) ) {
                                // если в этой мапе уже есть такой владелец и это не аноним:), то ++ его.
                                ownerNumberMap.put(rs1.getString(16),ownerNumberMap.get(rs1.getString(16))+1);
                            } else {
                                // если нет такого владельца, то создаём его или владельцу НЕИЗВЕСТЕН ставим =1.
                                ownerNumberMap.put(rs1.getString(16),1);
                            }
                        } else {
                            // если мы ещё не встречали такого КА, то создаём его
                            Map<String,Integer> ownerNumberMap = new HashMap<>();
                            // записываем любого владельца, даже, если он "НЕИЗВЕСТЕН". Просто потом не будем плюсовать его.
                            ownerNumberMap.put(rs1.getString(16),1);
                            kaOwnersMap.put(rs1.getString(19),ownerNumberMap);
                        }
                    }
                    // владельцы заполнены, можем сделать нужные комбобоксы с ними для таблицы


                } catch (ClassNotFoundException e1) {
                 //   e1.printStackTrace();
                    System.out.println(e1);
                } catch (SQLException e1) {
                //    e1.printStackTrace();
                    System.out.println(e1);
                }



                // проходим по всем строкам данных
                for (int row = 0; row < dataFromDB.size(); row++) {
                    ArrayList<String> strokaIzDB = dataFromDB.get(row);
                    // если стоит галка изменять строку, то
                    if ("1".equals(strokaIzDB.get(1))) {
                        // если не было отправлено в УРК, то ставим галку
                        if (!(Boolean)table.getValueAt(row,4)) {
                            table.setValueAt(true,row,4);
                        }
                        //скопируем назв КА из одной ещё в 3 колонки.
                        table.setValueAt(table.getValueAt(row,10),row,8);
                        table.setValueAt(table.getValueAt(row,10),row,9);
                        table.setValueAt(table.getValueAt(row,10),row,11);
                        // вставим 2 человеко-часа и КА
                        table.setValueAt(2.0f,row,12);
                        table.setValueAt("КА",row,13);
                        table.getCellEditor(row,7); // без этой строки не создаются combobox'ы и вылазит NPE.
                        table.setValueAt(comboBox.getItemAt(0),row,7);
                    }

                }
			}
		});
		// Создание кнопки записи
        write = new JButton("<html><font color=red>Перезаписать</font> все выделенные строки в столбце 2 <font color=red>в АСРК</font></html>");
		write.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

                try {
                    Connection connection = DriverManager.getConnection("jdbc:firebirdsql:172.16.120.209/3050:"+bdrkComboBox.getSelectedItem().toString()+"?encoding=win1251",loginField.getText(),new String(passwordField.getPassword()));
     //               Connection connection = DriverManager.getConnection("jdbc:firebirdsql:172.16.120.209/3050:"+"bdrk_test"+"?encoding=win1251","gki67","GKI_smol");
                    Statement statement = connection.createStatement();

                    // проходим по всем строкам данных
                    System.out.println("В "+bdrkComboBox.getSelectedItem().toString()+" пользователем "+loginField.getText()+" записано: ");
                    int counterOfEditedRows = 0;
                    for (int row = 0; row < dataFromDB.size(); row++) {
                        ArrayList<String> strokaIzDB = dataFromDB.get(row);
                        // если стоит галка изменять строку, то
                        if ("1".equals(strokaIzDB.get(1))) {
                            counterOfEditedRows++;
                            // записываем хрень в нужный столбец с указанным названием записи с номером id в таблицу RK_WORK_CONTROL_LOCAL

                            //для начала удалим [ххх шт.] из строки владельца, если тколичество владельцев приписано к концу строки
                            if (strokaIzDB.get(7).endsWith(" шт.]")) {
                                strokaIzDB.set(7,strokaIzDB.get(7).substring(0,strokaIzDB.get(7).lastIndexOf(" [")));
                            }
                            System.out.print("ID="+strokaIzDB.get(0)+" ");
                            statement.executeUpdate("UPDATE RK_WORK_CONTROL_LOCAL SET" +
                                    " IS_READY='"              +strokaIzDB.get(4)+
                                    "', PROTOCOL_DATE='"       +strokaIzDB.get(2)+
                                    "',PROTOCOL_NUMBER='"      +strokaIzDB.get(3)+
                                    "',ZADANIE_NUMBER='"       +strokaIzDB.get(5)+
                                    "',ZADANIE_DATE='"         +strokaIzDB.get(6)+
                                    "',RES_OWNER_NAME='"       +strokaIzDB.get(7)+
                                    "',RES_RK_REGION_TXT='"    +strokaIzDB.get(8)+
                                    "',RES_SP_DISTRICT_TXT='"  +strokaIzDB.get(9)+
                                    "',RES_NAS_PUNKT='"       +strokaIzDB.get(10)+
                                    "',RES_ADRESS='"          +strokaIzDB.get(11)+
                                    "',WORK_SPENDING='"       +strokaIzDB.get(12)+
                                    "',NOTES='"               +strokaIzDB.get(13)+
                                    "' WHERE ID_RK_WORK_CONTROL_LOCAL LIKE '"+strokaIzDB.get(0)+"'");
                            System.out.print("IS_READY="+strokaIzDB.get(4)+" ");
                            System.out.print("PROTOCOL_DATE="+strokaIzDB.get(2)+" ");
                            System.out.print("PROTOCOL_NUMBER="+strokaIzDB.get(3)+" ");
                            System.out.print("ZADANIE_NUMBER="+strokaIzDB.get(5)+" ");
                            System.out.print("ZADANIE_DATE="+strokaIzDB.get(6)+" ");
                            System.out.print("RES_OWNER_NAME="+strokaIzDB.get(7)+" ");
                            System.out.print("RES_RK_REGION_TXT="+strokaIzDB.get(8)+" ");
                            System.out.print("RES_SP_DISTRICT_TXT="+strokaIzDB.get(9)+" ");
                            System.out.print("RES_NAS_PUNKT="+strokaIzDB.get(10)+" ");
                            System.out.print("RES_ADRESS="+strokaIzDB.get(11)+" ");
                            System.out.print("WORK_SPENDING="+strokaIzDB.get(12)+" ");
                            System.out.print("NOTES="+strokaIzDB.get(13)+" ");
                            System.out.println();
                        } else {
                            System.out.println("  ничего...");
                        }
                    }
                    System.out.println("Строк вероятно изменено в АСРК: "+counterOfEditedRows+", а выведено на экран было: "+dataFromDB.size()+".");
                    statement.close();
                    connection.close();
                } catch (SQLException e1) {
                 //   e1.printStackTrace();
                    System.out.println(e1);
                }


			}
		});

		// редактор столбцов с датами
        table.setDefaultEditor(Date.class, new DateCellEditor());

		// Определение высоты строки
		table.setRowHeight(24);
		//определение предпочтительной ширины столбцов
        table.getColumnModel().getColumn(0).setPreferredWidth(30);
        table.getColumnModel().getColumn(3).setPreferredWidth(120);
        table.getColumnModel().getColumn(4).setPreferredWidth(100);
        table.getColumnModel().getColumn(7).setPreferredWidth(300);

        for (int i = 0; i < table.getColumnCount(); i++) {
            // если это слолбец с галочками, то не торгаем.
            if (table.getModel().getColumnClass(i) == Boolean.class) {continue;}

            // если это столбец со строками или Float
            if (table.getModel().getColumnClass(i) == String.class || table.getModel().getColumnClass(i) == Float.class) {
                table.getColumnModel().getColumn(i).setCellEditor(new DefaultCellEditor(new JTextField(){
                    // переопределение внутри JTextField
                    @Override
                    public Color getSelectedTextColor() {
                        return Color.RED;
                    }
                }){
                    // инициализатор внутри анонимного класса DefaultCellEditor
                    {
                        // редактировать одним кликом, а не двумя по-умолчанию.
                        setClickCountToStart(1);
                    }
                });
            }

            table.getColumnModel().getColumn(i).setCellRenderer(new DefaultTableCellRenderer(){
                // всё выравнивать по центру ячейки
                @Override
                public void setHorizontalAlignment(int alignment) {
                    super.setHorizontalAlignment(SwingConstants.CENTER);
                }
                //Отображение элемента с типом дата в таблице. Эсли это дата, то отображать по-человечески, а не по американски!
                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                    if (value instanceof Date) {
                        this.setText(sdf.format((Date) value));
                    } else if (value instanceof String && ((String) value).contains(" [")) {
                        this.setText(((String) value).substring(0,((String) value).lastIndexOf(" [")));
                    }
                    return this;
                }
            });
        }


		// Формирование интерфейса
		Box contents = new Box(BoxLayout.Y_AXIS);
		contents.add(new JScrollPane(table));
		getContentPane().add(contents);

		JPanel panelOf3Panels = new JPanel();
		panelOf3Panels.setLayout(new BoxLayout(panelOf3Panels,BoxLayout.Y_AXIS));

		JPanel queryPanel = new JPanel();
		queryPanel.add(queryLabel);
		queryPanel.add(queryField);

		JPanel loginAndPasswordPanel = new JPanel();
		loginAndPasswordPanel.add(loginLabel);
		loginAndPasswordPanel.add(loginField);
		loginAndPasswordPanel.add(passwordLabel);
		loginAndPasswordPanel.add(passwordField);
		loginAndPasswordPanel.add(bdrkLabel);
		loginAndPasswordPanel.add(bdrkComboBox);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.add(read);
		buttonsPanel.add(fill);
		buttonsPanel.add(write);

		panelOf3Panels.add(loginAndPasswordPanel);
		panelOf3Panels.add(queryPanel);
		panelOf3Panels.add(buttonsPanel);
		getContentPane().add(panelOf3Panels,"South");
		// Вывод окна на экран
		setBounds(150,300,1600,385);
		setVisible(true);
	}

    /**
     * Этот метод соединяется с АСРК и получает данные в ответе на запрос из БД.
     * Он сделан статическим, чтоб создавалось только одно соединение к БД.
     * @return Часть таблицы учёта РК, соответствующая запросу.
     */
    public ResultSet readFromDB(String query, String name, String pass, String bd) {
        try {
            System.out.println(name+" пытается прочитать данные из АСРК из БД "+bd);
            Class.forName("org.firebirdsql.jdbc.FBDriver");
            Connection connection = DriverManager.getConnection("jdbc:firebirdsql:172.16.120.209/3050:"+bd+"?encoding=win1251",name,pass);
            //получение имён всех таблиц в БД
            Statement statement = connection.createStatement();
            // только спутниковые протоколы
            ResultSet rs1 = statement.executeQuery("SELECT * FROM RK_WORK_CONTROL_LOCAL WHERE "+query);
            dataFromDB = new ArrayList<>();
            ArrayList<String> stringFromDB = new ArrayList<>();
            while (rs1.next()) {
                stringFromDB.add(rs1.getString(1));//0 id ID_RK_WORK_CONTROL_LOCAL
                stringFromDB.add("1");                        //1 перезаписать?
                stringFromDB.add(rs1.getString(2));//2 дата PROTOCOL_DATE
                stringFromDB.add(rs1.getString(4));//3 номер протокола PROTOCOL_NUMBER
                stringFromDB.add(rs1.getString(6));//4 УРК IS_READY
                stringFromDB.add(rs1.getString(13));//5 номер задания ZADANIE_NUMBER
                stringFromDB.add(rs1.getString(14));//6 дата задания ZADANIE_DATE
                stringFromDB.add(rs1.getString(16));//7 владелец RES_OWNER_NAME
                stringFromDB.add(rs1.getString(17));//8 КА RES_RK_REGION_TXT
                stringFromDB.add(rs1.getString(18));//9 КА RES_SP_DISTRICT_TXT
                stringFromDB.add(rs1.getString(19));//10 КА RES_NAS_PUNKT - заполнен.
                stringFromDB.add(rs1.getString(20));//11 КА RES_ADRESS
                stringFromDB.add(rs1.getString(47));//12 трудозатраты WORK_SPENDING
                stringFromDB.add(rs1.getString(48));//13 примечание NOTES
                dataFromDB.add(stringFromDB);
                stringFromDB = new ArrayList<>();
            }
            urkIsReady = new String[dataFromDB.size()];
            for (int i = 0; i < dataFromDB.size(); i++) {
                stringFromDB = dataFromDB.get(i);
                urkIsReady[i] = stringFromDB.get(4);
            }
            connection.close();
            return rs1;
        }catch (Exception e) {
            class ErrorFrame extends JDialog {
                public ErrorFrame(Frame owner) {
                    super(owner, "Ошибка",true);
                    JLabel text = new JLabel();
                    text.setText("<html><h1><i>Скорее всего не получилось угадать логин и пароль от АСРК! Попробуй ещё. </i></h1><hr>"+e.toString()+"</html>");
                    add(text);
                    JButton okButton = new JButton("Хорошо, ща подправлю.");
                    okButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            setVisible(false);
                        }
                    });
                    JPanel panel = new JPanel();
                    panel.add(okButton);
                    add(panel,BorderLayout.SOUTH);
                    setBounds(150+400,300+100,1000,160);
                }
            }
            ErrorFrame errFrame = new ErrorFrame(this);
            errFrame.setVisible(true);
         //   e.printStackTrace();
            System.out.println(e);
        }
        return null;
    }

	// Модель данных
	class SimpleModel extends AbstractTableModel {
		private static final long serialVersionUID = 1L;

		// можно ли редактировать?
        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            // можно всё, кроме порядкового номера
            return columnIndex != 0;
        }
        // Количество строк
		@Override
		public int getRowCount() {
            if (dataFromDB != null) {
                return dataFromDB.size();
            } else {
                return 0;
            }
		}

		// Количество столбцов
		@Override
		public int getColumnCount() {
			return 14;
		}
		//названия столбцов
		@Override
		public String getColumnName(int column) {
			return columnsHeader[column];
		}
		// Тип хранимых в столбцах данных
		@Override
		public Class<?> getColumnClass(int column) {
			switch (column) {
				case 0: return String.class;
				case 1: return Boolean.class;
				case 2: return Date.class;
				case 4: return Boolean.class;
				case 6: return Date.class;
				case 12: return Float.class;
				default: return String.class;
			}
		}

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            ArrayList<String> strokaIzDB = dataFromDB.get(rowIndex);
            if (aValue instanceof Boolean) {
                if ("1".equals(strokaIzDB.get(columnIndex))) {
                    if ("1".equals(urkIsReady[rowIndex]) && columnIndex == 4) {
                       // если было отправлено в урк, то галку снимать нельзя.
                        System.out.println("Запрещено снимать галку 'отправлено в УРК', если она уже стоит!");
                    } else {
                        // если ещё не было отправлено в УРК или это не столбец УРКа, а другой, то можем ставить или не ставить по нашему выбору
                        strokaIzDB.set(columnIndex,"0");
                    }
                } else {
                    strokaIzDB.set(columnIndex,"1");
                }
            } else if (aValue instanceof String) {
                // это не работает, т.к. для столбца String назначен DefaultCellEditor для редактирования одним кликом
                strokaIzDB.set(columnIndex,(String) aValue);
            } else if (aValue instanceof Date) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                strokaIzDB.set(columnIndex,sdf.format((Date) aValue));
            } else if (aValue instanceof Float) {
                // это не работает, т.к. для столбца String назначен DefaultCellEditor для редактирования одним кликом
                // пренесём это всё в метод getValueAt, там проверится...
                if ((Float) aValue > 0 && (Float)aValue < 1000000) {
                    strokaIzDB.set(columnIndex,String.format(Locale.ENGLISH, "%.2f",(Float) aValue));
                } else {
                    System.out.println("Введено ["+aValue+"]. Не пойдет вводить число < 0 и > 999999! Введём-ка вместо этого лучше 'null' :)");
                    strokaIzDB.set(columnIndex,"null");
                }
            } else {
                System.out.println("Это не класс String, Boolean, Float или Date, а какая-то хрень, как это редактировать???");
            }
            fireTableCellUpdated(rowIndex,columnIndex);
        }

        // Функция определения данных ячейки
		@Override
		public Object getValueAt(int row, int column)
		{

		    // Данные для стобцов
			switch (column) {
				case 0: return "" + (row+1);
				case 1: return "1".equals(dataFromDB.get(row).get(1));
                case 2: {
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        return df.parse(dataFromDB.get(row).get(2));
                    } catch (ParseException e) {
                        System.out.println("Что-то не то с датой..., не соответствует yyyy-mm-dd");
                      //  e.printStackTrace();
                        System.out.println(e);
                    }
                }
                case 3: return dataFromDB.get(row).get(3);
                case 4: return "1".equals(dataFromDB.get(row).get(4));
                case 5: return dataFromDB.get(row).get(5);
                case 6: {
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        return df.parse(dataFromDB.get(row).get(6));
                    } catch (ParseException e) {
                        System.out.println("Что-то не то с датой..., не соответствует yyyy-mm-dd");
                      //  e.printStackTrace();
                        System.out.println(e);
                    }
                }
                case 7: return dataFromDB.get(row).get(7); // влад
                case 8: return dataFromDB.get(row).get(8);
                case 9: return dataFromDB.get(row).get(9);
                case 10: return dataFromDB.get(row).get(10);
                case 11: return dataFromDB.get(row).get(11);
                case 12: {
                    try {
                        if (Float.parseFloat(dataFromDB.get(row).get(12)) > 0 && Float.parseFloat(dataFromDB.get(row).get(12)) < 1000000) {
                            return Float.parseFloat(dataFromDB.get(row).get(12));
                        } else {
                            System.out.println("В трудозатратах в getValueAt пытается быть: ["+dataFromDB.get(row).get(12)+"]. Не пойдет вводить число < 0 и > 999999! Введём-ка вместо этого лучше 'null', как-будто ничего и не было... :)");
                            return null;
                        }
                    } catch (NumberFormatException ex) {
                        System.out.println("parseFloat не смог расчехлить число["+dataFromDB.get(row).get(12)+"]. Ну это же не число! Пишем 'null'.");
                        return null;
                    } catch (NullPointerException npe) {
                        System.out.println("Случилось NPE. В Float.parseFloat(dataFromDB.get(row).get(12)), в ячейке трудозатрат записан 'null'");
                        return null;
                    }

                }
                case 13: return dataFromDB.get(row).get(13);
			}
			return "-";
		}
	}

    public static void main(String[] args) {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy (EEEE)");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+3"));
        startDate = new Date();
        String nowDate = sdf.format(startDate);
        System.out.println("Программа запущена в "+nowDate+".");

        ASRKBdChangerFrame frame = new ASRKBdChangerFrame();

        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                Date endOfProgramDate = new Date();
                String endOfProgramDateString = sdf.format(endOfProgramDate);
                System.out.print("Программа отработала "+(endOfProgramDate.getTime()-startDate.getTime())/1000+" сек. и была закрыта в "+endOfProgramDateString+".");
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println();
                System.exit(0);
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

        // включать или нет АВТО-режим нажатия всех кнопок по очереди? Если равно "1", то включать.
        String automaticMode = "";

        if (args.length > 0) {
            for (String s : args) {
                if (s.contains("login=")) {
                    System.out.print(" Логин "+s.substring(6));
                    frame.loginField.setText(s.substring(6));
                }
                if (s.contains("password=")) {
          //          System.out.print(" Пароль ***");
                    frame.passwordField.setText(s.substring(9));
                }
                if ("bd=bdrk".equals(s)) {
                    System.out.print(" БДРК = bdrk ");
                    frame.bdrkComboBox.setSelectedItem(frame.bdrkComboBox.getItemAt(0));
                }
                if ("bd=bdrk_test".equals(s)) {
                    System.out.print(" БДРК = bdrk_test ");
                    frame.bdrkComboBox.setSelectedItem(frame.bdrkComboBox.getItemAt(1));
                }
                if (s.contains("automaticMode=")) {
                    if ("1".equals(s.substring(14))) {
                        automaticMode = "1";
                        System.out.print(" АВТОМАТИЧЕСКИЙ РЕЖИМ!");
                    }
                }
            }
            System.out.println();
        } else {
            System.out.println("Ни один параметр не введён. Вероятно Вы не знали, что с помощью них работать очень удобно! Доступные параметры: ");
            System.out.println();
            System.out.println("login= - логин от АСРК");
            System.out.println("password= - пароль от АСРК");
            System.out.println();
            System.out.println("С какой БДРК работать? ");
            System.out.println("bd=bdrk - БДРК");
            System.out.println("bd=bdrk_test - тестовая БДРК");
            System.out.println();
            System.out.println("Установить автоматический режим: ");
            System.out.println("automaticMode=1");
            System.out.println("При этом просто по очереди сами нажмутся 3 кнопки и программа самозакроется.");
            System.out.println();
            System.out.println("Например, в bat-файле может быть:");
            System.out.println("java -jar ASRKBdChangerFrame.1.0.jar login=gki67 password=qwerty11 bd=bdrk automaticMode=1 >> log.txt");
            System.out.println("Тогда ещё и лог-файл log.txt сохранится, причём будет дописываться каждый раз. Но можно и без него. Тогда >> log.txt не надо.");
        }

        if ("1".equals(automaticMode)) {
            frame.read.doClick();
            frame.fill.doClick();
            frame.write.doClick();
            frame.dispatchEvent(new WindowEvent(frame,WindowEvent.WINDOW_CLOSING));
        }

	}
}

class DateCellEditor extends AbstractCellEditor implements TableCellEditor {
    private static final long serialVersionUID = 2L;
    // Редактор
    private JSpinner editor;
    // Конструктор
    public DateCellEditor() {
        // Настройка прокручивающегося списка
        SpinnerDateModel model = new SpinnerDateModel(new Date(), null, null, Calendar.DAY_OF_MONTH);
        editor = new JSpinner(model);
        editor.setEditor(new JSpinner.DateEditor(editor,"dd.MM.yyyy"));
    }
    // Метод получения компонента для прорисовки
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        // Изменение значения
        editor.setValue(value);
        return editor;
    }
    // Функция текущего значения в редакторе
    public Object getCellEditorValue() {
        return editor.getValue();
    }
}