package com.gkisoft.gcst.asrkThreater;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by k.golovkin on 13.08.17.
 */
public class AsrkBdChanger {
    public static void main(String[] args) {
        try {
            Class.forName("org.firebirdsql.jdbc.FBDriver");
            Connection connection = DriverManager.getConnection("jdbc:firebirdsql:172.16.120.209/3050:bdrk_test?encoding=win1251","gki67","GKI_smol");

            //получение имён всех таблиц в БД
 /*           DatabaseMetaData databaseMetaData = connection.getMetaData();
            ResultSet rs = databaseMetaData.getTables(null,null,null,null);

           while (rs.next()) {
                // что за таблица. Её описание.
                System.out.println(String.format("%-35s %-12s %-20s",rs.getString(3),rs.getString(4),rs.getString(5)));

                Statement statement = connection.createStatement();
                try {
                    ResultSet rs1 = statement.executeQuery("SELECT * FROM "+rs.getString(3));
        //            System.out.println("SQL: "+"SELECT * FROM "+rs.getString(3)+" колонок: "+rs1.getMetaData().getColumnCount()+" =============================================================================================================================");
                    //заголовки полей таблицы
                    for (int i = 1; i <= rs1.getMetaData().getColumnCount(); i++) {
                        System.out.print(String.format("%-25s ",rs1.getMetaData().getColumnName(i)));
                    }
                    System.out.println("");

                    int i = rs1.getMetaData().getColumnCount();
                    // данные таблиц
                    int stop = 1;
                    while (rs1.next()) {
                        //пропускаем метрологию и т.д. ...там много фигни
                        if (rs.getString(3).equals("RK_RKO_METROLOGY") ||
                                rs.getString(3).equals("RK_EXTRA_DEV_METROLOGY") ||
                                rs.getString(3).equals("RESULT_BLOB")) {
                            stop++;
                        }
                        if (stop >= 2) {
                            System.out.print("Больше 1 записей, прерываем...");
                            System.out.println();
                   //         System.out.println();
                   //         System.out.println();
                            stop = 1;
                            break;
                        } else {
                            stop++;
                        }
                        System.out.print(stop+" ");
                        for (int j = 1; j <= i; j++) {
                            System.out.print(String.format("%-25s ", rs1.getString(j)));
                        }
                        System.out.println("");
                    }
              //      System.out.println();
            //        System.out.println();
                    System.out.println();
                } catch (SQLException ex) {
                    System.out.println("Ошибка SQL: "+ex);
                }

            }
*/
            System.out.println("==================================================================================================================");

            Statement statement = connection.createStatement();

            // только спутниковые протоколы
            ResultSet rs1 = statement.executeQuery("SELECT * FROM RK_WORK_CONTROL_LOCAL WHERE PROTOCOL_NUMBER LIKE '%39100%' AND PROTOCOL_DATE LIKE '%'");
            //заголовки полей таблицы
            System.out.println(rs1.getMetaData().getColumnCount()+" столбцов.");
            for (int i = 1; i <= rs1.getMetaData().getColumnCount(); i++) {
                System.out.print(String.format(i+"[%-35s "+JDBCType.valueOf(rs1.getMetaData().getColumnType(i)).getName()+"]",rs1.getMetaData().getColumnName(i)));
            }

            System.out.println("");

            // данные таблиц
            int k = 0;
            Map<String,Map<String,Integer>> kaOwnersMap = new HashMap<>(); // мапа, содержащая всех владельцев КА и их кол-во для каждого КА, чтоб потом искать лидера:)
            while (rs1.next()) {
                System.out.print(k+": ");
                for (int i = 1; i <= rs1.getMetaData().getColumnCount(); i++) {

                    if (rs1.getObject(i) != null) {
                        System.out.print(String.format(i+"[ %s %s]", rs1.getString(i),rs1.getObject(i).getClass()));
                    } else {
                        System.out.print(String.format(i+"[ %s %s]", rs1.getString(i),"null"));
                    }
                }
                System.out.println();
                break;
       /*         k++;

                if ( kaOwnersMap.containsKey(rs1.getString(19)) ) {
                    // если RES_NAS_PUNKT (19 столбец == назв. КА) содержится в мапе
                    // достаём мапу владельцев
                    Map<String,Integer> ownerNumberMap = kaOwnersMap.get(rs1.getString(19));
                    if ( ownerNumberMap.containsKey(rs1.getString(16)) && !"НЕИЗВЕСТЕН".equals(rs1.getString(16)) ) {
                        // если в этой мапе уже есть такой владелец и это не аноним:), то ++ его.
                        ownerNumberMap.put(rs1.getString(16),ownerNumberMap.get(rs1.getString(16))+1);
                    } else {
                        // если нет такого владельца, то создаём его или владельцу НЕИЗВЕСТЕН ставим =1.
                        ownerNumberMap.put(rs1.getString(16),1);
                    }
                } else {
                    // если мы ещё не встречали такого КА, то создаём его
                    Map<String,Integer> ownerNumberMap = new HashMap<>();
                    // записываем любого владельца, даже, если он "НЕИЗВЕСТЕН". Просто потом не будем плюсовать его.
                    ownerNumberMap.put(rs1.getString(16),1);
                    kaOwnersMap.put(rs1.getString(19),ownerNumberMap);
                }
       */     }

            for (Map.Entry<String,Map<String,Integer>> e:kaOwnersMap.entrySet()) {
                System.out.println("КА: "+e.getKey());
                for (Map.Entry<String,Integer> ee:e.getValue().entrySet()) {
                    System.out.println("      Кол-во: "+ee.getValue()+" Влад: "+ee.getKey());
                }
            }

            // записываем хрень
            statement.executeUpdate("UPDATE RK_WORK_CONTROL_LOCAL SET IS_READY=0 WHERE PROTOCOL_NUMBER LIKE '%39100%' AND PROTOCOL_DATE LIKE '2017-07-02'");



            connection.close();
        }catch (Exception e) {
            e.printStackTrace();
        }


    }
}
