package com.gkisoft.gcst.excelreader;

/**
 *  Created by pc7 on 07.02.2017.
 */
public enum Polarization {
    H("горизонтальная"), V("вертикальная"), L("левая"), R("правая"), UNKNOWN("неизвестная");

    private String description;

     /** это контруктор, который создает нужную поляризацию из списка, он всегда приватный,
     * доступен только внутри enum.
     * */
    Polarization (String polarizationStr) {
        this.description = polarizationStr;
    }

     /** этот метод добывает из enum правильную строчку описания,
     * которая может начинаться с цифры и иметь пробелы.
     * */
    public String getStringDescription() {
        return description;
    }

    /** Этот метод определяет поляризацию сигнала из переданной строки.*/
    public Polarization parsePolarization (String stringToParse) {
        if ( stringToParse.contains("R") || stringToParse.contains("r") ) {
            return R;
        } else if ( stringToParse.contains("L") || stringToParse.contains("l") ) {
            return L;
        } else if ( stringToParse.contains("H") || stringToParse.contains("h") ) {
            return H;
        } else if ( stringToParse.contains("V") || stringToParse.contains("v") ) {
            return V;
        } else {
            return UNKNOWN;
        }
    }
}

