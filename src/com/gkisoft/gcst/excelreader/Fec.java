package com.gkisoft.gcst.excelreader;

/**
 *  Created by pc7 on 07.02.2017.
 */

    public enum Fec {
    F1_4("1/4"), F1_3("1/3"), F2_5("2/5"), F1_2("1/2"), F3_5("3/5"),
    F2_3("2/3"), F3_4("3/4"), F4_5("4/5"), F5_6("5/6"), F7_8("7/8"),
    F8_9("8/9"), F9_10("9/10"), AUTO("авто");

    private String description;

     /**это контруктор, который создает нужный FEC из списка, он всегда приватный,
     * доступен только внутри enum
     * */
    Fec(String str) {
        description = str;
    }

     /** этот метод добывает из enum правильную строчку описания,
     * которая может начинаться с цифры и иметь пробелы.
     * */
    public String getStringDescription(){
        return description;
    }

    /** Этот метод определяет FEC из переданной строки.*/
    public Fec parseFec (String stringToParse){
        if ( "3/4".equals(stringToParse) ) {
            return F3_4;
        } else if ( "5/6".equals(stringToParse) ) {
            return F5_6;
        } else if ( "7/8".equals(stringToParse) ) {
            return F7_8;
        } else if ( "8/9".equals(stringToParse) ) {
            return F8_9;
        } else if ( "9/10".equals(stringToParse) ) {
            return F9_10;
        } else if ( "4/5".equals(stringToParse) ) {
            return F4_5;
        } else if ( "2/3".equals(stringToParse) ) {
            return F2_3;
        } else if ( "3/5".equals(stringToParse) ) {
            return F3_5;
        } else if ( "1/2".equals(stringToParse) ) {
            return F1_2;
        } else if ( "2/5".equals(stringToParse) ) {
            return F2_5;
        } else if ( "1/3".equals(stringToParse) ) {
            return F1_3;
        } else if ( "1/4".equals(stringToParse) ) {
            return F1_4;
        } else {
            return AUTO;
        }
    }
}