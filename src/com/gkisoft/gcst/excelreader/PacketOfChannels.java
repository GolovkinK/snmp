package com.gkisoft.gcst.excelreader;

import java.util.Date;

/**
 *  Created by pc7 on 07.02.2017.
 */
public class PacketOfChannels {
    private final int id; //уникальный идентификатор, при создании первого
    private static int count; //общий счетчик объектов.
    private String satName; //название КА
    private Polarization pol; // поляризация
    private long realFreq; //частота, Гц
    private long lBandFreq; // частота в L-диапазоне, Гц
    private Date date; //дата, когда излучение было обнаружено
    private Modulation modulation; //модуляция
    private int symbolRate; // Симв. скорость, Бод
    private Fec fec; //FEC
    private DvbType dvbType; //Тип сигнала DVB-S или DVB-S2
    private String filePathFrom; // Путь к файлу, из которого достали пакет каналов

    /** Это усеченный конструктор, для всех полей, кооторые можно заполнить изнутри БРКСовского xls файла.*/
    public PacketOfChannels(long realFreq, long lBandFreq, Date date, Modulation modulation, int symbolRate, Fec fec, DvbType dvbType) {
        this.satName = "неизвестный";
        this.pol = Polarization.UNKNOWN;
        this.realFreq = realFreq;
        this.lBandFreq = lBandFreq;
        this.date = date;
        this.modulation = modulation;
        this.symbolRate = symbolRate;
        this.fec = fec;
        this.dvbType = dvbType;
        this.id = count++;
        this.filePathFrom = "Путь к файлу не определён.";
    }

    @Override
    public String toString() {
        return String.format("Пакет каналов %3d: КА %11s; Поляризация %14s; Обнаружен %td.%tm.%tY; Частота %9.3f МГц; Частота L %8.3f МГц;" +
                        " Симв.скор %5d кБод; модуляция %6s; FEC %4s; Тип сигнала %s.",
                id+1, satName, pol.getStringDescription(), date, date, date, (double)realFreq/1000000, (double)lBandFreq/1000000,
                symbolRate/1000, modulation.getStringDescription(), fec.getStringDescription(), dvbType.getStringDescription());
    }


    /** Путь к файлу, из которого достали пакет каналов.*/
    public String getFilePathFrom() {
        return filePathFrom;
    }

    /** Установить путь к файлу, из которого достали пакет каналов.*/
    public void setFilePathFrom(String filePathFrom) {
        this.filePathFrom = filePathFrom;
    }

    /** Тип сигнала DVB-S или DVB-S2.*/
    public DvbType getDvbType() {
        return dvbType;
    }

    /** Тип сигнала DVB-S или DVB-S2.*/
    public void setDvbType(DvbType dvbType) {
        this.dvbType = dvbType;
    }

    /** Нзвание КА.*/
    public String getSatName() {
        return satName;
    }

    /** Нзвание КА.*/
    public void setSatName(String satName) {
        this.satName = satName;
    }

    /** Поляризация.*/
    public Polarization getPol() {
        return pol;
    }

    /** Поляризация.*/
    public void setPol(Polarization pol) {
        this.pol = pol;
    }

    /** Частота, Гц.*/
    public void setRealFreq(long realFreq) {
        this.realFreq = realFreq;
    }

    /** Частота, Гц.*/
    public long getRealFreq() {
        return realFreq;
    }

    /** Частота в L-диапазоне, Гц. */
    public void setlBandFreq(long lBandFreq) {
        this.lBandFreq = lBandFreq;
    }

    /** Частота в L-диапазоне, Гц. */
    public long getlBandFreq() {
        return lBandFreq;
    }

    /** Дата, когда излучение было обнаружено.*/
    public void setDate(Date date) {
        this.date = date;
    }

    /** Дата, когда излучение было обнаружено.*/
    public Date getDate() {
        return date;
    }

    /** Модуляция.*/
    public void setModulation(Modulation modulation) {
        this.modulation = modulation;
    }

    /** Модуляция.*/
    public Modulation getModulation() {
        return modulation;
    }

    /** Симв. скорость, Бод.*/
    public void setSymbolRate(int symbolRate) {
        this.symbolRate = symbolRate;
    }

    /** Симв. скорость, Бод.*/
    public int getSymbolRate() {
        return symbolRate;
    }

    /** FEC.*/
    public void setFec(Fec fec) {
        this.fec = fec;
    }

    /** FEC.*/
    public Fec getFec() {
        return fec;
    }
}
