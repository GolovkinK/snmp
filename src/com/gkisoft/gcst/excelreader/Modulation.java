package com.gkisoft.gcst.excelreader;

/**
 *  Created by pc7 on 07.02.2017.
 */
public enum Modulation {
    QPSK("QPSK"), PSK8("8 PSK"), ACM("ACM"), CCM("CCM"), APSK16("16APSK"), UNKNOWN("неизв.");

    private String description;

     /** это контруктор, который создает нужную модуляцию из списка, он всегда приватный,
     * доступен только внутри enum.
     * * */
    Modulation (String mod) {
        this.description = mod;
    }

     /** этот метод добывает из enum правильную строчку описания,
     * которая может начинаться с цифры и иметь пробелы.
     * */
    public String getStringDescription() {
        return description;
    }

    /** Этот метод определяет тип модуляции из переданной строки.*/
    public Modulation parseModulation(String stringToParse){
        if ( "ФМ4".equals(stringToParse) ) {
            return Modulation.QPSK;
        } else if ( "ФМ8".equals(stringToParse) ) {
            return Modulation.PSK8;
        } else if ( "ССМ".equals(stringToParse) ) {
            return Modulation.CCM;
        } else if ( "АСМ".equals(stringToParse) ) {
            return Modulation.ACM;
        } else {
            return Modulation.UNKNOWN;
        }
    }
}
