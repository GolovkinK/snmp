package com.gkisoft.gcst.excelreader;

/**
 *  Created by pc7 on 07.02.2017.
 */
public enum DvbType {
    DVBS("DVB-S"), DVBS2("DVB-S2"), UNKNOWN("Неизвестный");

    private String description;

     /**это контруктор, который создает нужный тип сигнала из списка, он всегда приватный,
     * доступен только внутри enum
     * */
    DvbType (String dvbType) {
        this.description = dvbType;
    }

    /** этот метод добывает из enum правильную строчку описания,
     * которая может начинаться с цифры и иметь пробелы*/
    public String getStringDescription() {
        return description;
    }

    /** Этот метод определяет тип DVB сигнала из переданной строки.*/
    public DvbType parseDvb (String stringToParse){
        if ( "DVBS2".equals(stringToParse) ) {
            return DVBS2;
        } else if ( "Витерби 7 (3/4)".equals(stringToParse) ) {
            return DVBS;
        } else {
            return UNKNOWN;
        }
    }
}

