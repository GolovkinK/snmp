package com.gkisoft.gcst.excelreader;

import com.gkisoft.gcst.gkisnmp.SnmpReader;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;

/**
 *  Created by gki on 07.02.2017.
 *  It is GKI Channel Searching Tool
 */
public class ExcelReader {
    private static LinkedList<File> resultXls = new LinkedList<>(); //список всех файлов с расширением xls, кроме OUTPUTFILENAME
    private static LinkedList<PacketOfChannels> resultListOfChannels = new LinkedList<>(); //список всех найденных в .xls файлах пакетов каналов.
    private static final String OUTPUTFILENAME = "out_DVB_From_BRKS.xls";
    private static final String CURRENTDIRECTORY = "user.dir";
    private static final String FILESEPARATOR = "file.separator";

    public static void main(String[] args) {
        System.out.println("Мы сейчас находимся скорей всего в папке: "+System.getProperty(CURRENTDIRECTORY));
        System.out.println("");
        System.out.println("Ищем все XLS файлы, находящиеся в ней и во вложенных папках...");
        File f = new File(System.getProperty(CURRENTDIRECTORY));
        findXlsFilesRecurseFromFolder(f);

        System.out.println("Найдено .xls файлов: "+resultXls.size()+" шт.");
        System.out.println("");

        for (int i = 0; i < resultXls.size(); i++) {
            System.out.println("Обрабатываем файл "+(i+1)+" из "+resultXls.size()+": "+resultXls.get(i).toString());
            try {
                //метод searchDvbFromExcel возвращает список пакетов каналов из текущего переданного файла.
                LinkedList<PacketOfChannels> l = searchDvbFromExcel(resultXls.get(i).toString()); //"brks/YAMAL-401_YAMAL-401_L_5150_tmp.xls"
                resultListOfChannels.addAll(l);
                System.out.println("Найдено пакетов каналов: "+l.size()+".");
                System.out.println("");
            } catch (IOException ex) {
                System.out.println("Произошла ошибка чтения файла: "+resultXls.get(i).toString());
                System.out.println("Файл: "+resultXls.get(i).toString()+" не обработан.");
                ex.printStackTrace();
            }
        }

        System.out.println("Все найденно пекетов: "+resultListOfChannels.size()+".");
        for (PacketOfChannels ch : resultListOfChannels) {
            System.out.println(ch.toString());
        }
        System.out.println("");
        System.out.println("Запишем все эти каналы в файл \""+OUTPUTFILENAME+"\".");
        try {
            writeIntoExcel(OUTPUTFILENAME, resultListOfChannels);
        } catch (IOException ex) {
            System.out.println("Произошла ошибка записи файла: "+System.getProperty(CURRENTDIRECTORY)+System.getProperty(FILESEPARATOR)+OUTPUTFILENAME);
            ex.printStackTrace();
        }
    }

    /** Этот метод записывает все строчки из коллекции пакетов каналов в файл с указанным именем.*/
    private static void writeIntoExcel(String fileName, LinkedList<PacketOfChannels> list) throws IOException {
        HSSFWorkbook myExcelBook = new HSSFWorkbook(ExcelReader.class.getResourceAsStream("/resources/BRKS_template.xls"));
        HSSFSheet myExcelSheet = myExcelBook.getSheetAt(0);
        //заполняем все ячейки информацией из пакетов каналов
        for (int i = 0; i < list.size(); i++) {
            //сохраним стили ячеек, которые будем заполнять в массив
            CellStyle[] style = new CellStyle[10];
            for (int j = 0; j < 10; j++) {
                style[j] = myExcelSheet.getRow(5).getCell(j).getCellStyle();
            }
            //создадим новую строку
            HSSFRow row = myExcelSheet.createRow(i + 4);
            //по очереди создаем ячейки, устанавливаем стиль, устанавливаем значение
            Cell date = row.createCell(0);
            date.setCellStyle(style[0]);
            date.setCellValue(list.get(i).getDate());
            Cell satName = row.createCell(1);
            satName.setCellStyle(style[1]);
            satName.setCellValue(list.get(i).getSatName());
            Cell pol = row.createCell(2);
            pol.setCellStyle(style[2]);
            pol.setCellValue(list.get(i).getPol().getStringDescription());
            Cell realFreq = row.createCell(3);
            realFreq.setCellStyle(style[3]);
            realFreq.setCellValue(list.get(i).getRealFreq() / 1000d);
            Cell lBandFreq = row.createCell(4);
            lBandFreq.setCellStyle(style[4]);
            lBandFreq.setCellValue(list.get(i).getlBandFreq() / 1000d);
            Cell symbolRate = row.createCell(5);
            symbolRate.setCellStyle(style[5]);
            symbolRate.setCellValue(list.get(i).getSymbolRate() / 1000d);
            Cell mod = row.createCell(6);
            mod.setCellStyle(style[6]);
            mod.setCellValue(list.get(i).getModulation().getStringDescription());
            Cell fec = row.createCell(7);
            fec.setCellStyle(style[7]);
            fec.setCellValue(list.get(i).getFec().getStringDescription());
            Cell signalType = row.createCell(8);
            signalType.setCellStyle(style[8]);
            signalType.setCellValue(list.get(i).getDvbType().getStringDescription());
            Cell filePath = row.createCell(9);
            filePath.setCellStyle(style[9]);
            filePath.setCellValue(list.get(i).getFilePathFrom());
        }
        // Записываем всё в файл
        myExcelBook.write(new FileOutputStream(fileName));
        myExcelBook.close();
    }

    /** Этот метод должен найти все строки в переданном файле, содержащие "MPEG_TS" и взять от них всю доступную информацию.*/
    private static LinkedList<PacketOfChannels> searchDvbFromExcel (String fileName) throws IOException {
        LinkedList<PacketOfChannels> list = new LinkedList<>();
        boolean isSatNameAndPolAlreadyDefined = false; //уже парсили имя файла в поиске имени КА и поляризации?
        String satName = "неизвестный";
        Polarization pol = Polarization.UNKNOWN;

        FileInputStream inputStream = new FileInputStream(fileName); //открываем отдельно, чтоб потом можно было закрыть.
        HSSFWorkbook wb = new HSSFWorkbook(inputStream);
        //пепебираем все ячейки во всех строках на всех листах
        for (Sheet sheet : wb ) {
            for (Row row : sheet) {
                for (Cell cell : row) {
                    PacketOfChannels packet = parseDataFromExcelCell(row, cell); //это пока неполный пакет
                    if ( packet != null ) { //если нашли хоть какой пакет
                        if (!isSatNameAndPolAlreadyDefined) { //если ещё не опрделено название КА и поляризация, то запомним
                            satName = parseSatNameFromFileName(fileName);
                            pol = parsePolFromFileName(fileName);
                            isSatNameAndPolAlreadyDefined = true;
                        }
                        //дописываем недостающие данные в пакет.
                        packet.setSatName(satName);
                        packet.setPol(pol);
                        packet.setFilePathFrom(fileName);
                        //и отправляем пакет в общий спмсок пакетов.
                        list.add(packet);
                    }
                }
            }
        }
        wb.close();
        inputStream.close();
        return list;
    }

     /** Этот метод возвращает пакет каналов, заполненный данными из Excel,
     * но не полностью, для счастья не хватает пути, имени КА и поляризации.
     * */
    private static PacketOfChannels parseDataFromExcelCell (Row row, Cell cell) {
        if ("MPEG_TS".equals(cell.toString())) { //если нашли хоть что-то полезное
            long realFreq = 1000 * ((long)row.getCell(0).getNumericCellValue());
            long lBandFreq = 1000 * ((long)row.getCell(1).getNumericCellValue());
            Date date = row.getCell(2).getDateCellValue();
            Modulation modulation = Modulation.UNKNOWN.parseModulation(row.getCell(5).toString());
            int symbolRate = 1000 * ((int) row.getCell(6).getNumericCellValue());
            Fec fec = Fec.AUTO.parseFec(row.getCell(7).toString());
            DvbType dvbType = DvbType.UNKNOWN.parseDvb(row.getCell(8).toString());
            //создаём новый пакет каналов
            return new PacketOfChannels(realFreq,lBandFreq,date, modulation, symbolRate, fec, dvbType);
        } else {
            return null;
        }
    }

    /** Метод возвращает имя КА, полученное из имени файла БРКСа.*/
    private static String parseSatNameFromFileName (String fileName) {
        String fileSeparator=System.getProperty(FILESEPARATOR);
        String satNameFromFileName = "неизвестный";
        if ( fileName.contains("_") && fileName.contains(fileSeparator) ) {
            //оставляем от всего пути только имя файла до последнего '_'.
            satNameFromFileName = fileName.substring(fileName.lastIndexOf(fileSeparator)+1,fileName.lastIndexOf('_'));
            if ( satNameFromFileName.contains("_") ) {
                satNameFromFileName = satNameFromFileName.substring(0, satNameFromFileName.indexOf('_'));
            } else {
                System.out.println("Имя КА: \"неизвестный\", т.к. в имени файла \""+satNameFromFileName+"\" не нашлось \"_\".");
            }
        } else {
            System.out.println("Имя КА: \"неизвестный\", т.к. в пути к файлу \""+fileName+"\" не нашлись \"_\".");
        }
        return satNameFromFileName;
    }

    /** Метод возвращает поляризацию, полученную из имени файла БРКСа.*/
    private static Polarization parsePolFromFileName (String fileName) {
        Polarization p = Polarization.UNKNOWN;
        char ch;
        String fileSeparator=System.getProperty(FILESEPARATOR);
        if ( fileName.contains("_") && fileName.contains(fileSeparator) ) {
            //оставляем от всего пути только имя файла до последнего '_'.
            String satNameFromFileName = fileName.substring(fileName.lastIndexOf(fileSeparator)+1,fileName.lastIndexOf('_'));
            //определяем поляризацию из имени файла по буквам.
            if (satNameFromFileName.contains("_")) {
                ch = satNameFromFileName.charAt(satNameFromFileName.lastIndexOf('_') - 1);
                p = p.parsePolarization(String.valueOf(ch));
            } else {
                System.out.println("Поляризация: \"неизвестная\", т.к. в имени \""+satNameFromFileName+"\" не нашлись '_'.");
            }
        } else {
            System.out.println("Поляризация: \"неизвестная\", т.к. в пути к файлу \""+fileName+"\" не нашлись '_'.");
        }
        return p;
    }

     /** Этот метод ищет все файлы рекурсивно в переданной папке иподпапках,
     * и отправляет каждый найденный файл на обработку
     * */
    private static void findXlsFilesRecurseFromFolder(File folder)
    {
        File[] folderEntries = folder.listFiles();
        if (folderEntries != null) { //т.к. folder.listFiles() может вернуть null.
            for (File entry : folderEntries) {
                if (entry.isDirectory()) {
                    //если это директория, то пускаем рекурсивный поиск
                    findXlsFilesRecurseFromFolder(entry);
                } else {
                    // иначе нам попался файл, обрабатываем его
                    processXlsFile(entry);
                }
            }
        }
    }

    /** Этот метод добавляет все файлы с расширением xls в коллекцию resultXls,
     * кроме OUTPUTFILENAME, для которого выдаются предупреждения в зависимости от того, где он расположен.
     * */
    private  static void processXlsFile(File entry) {
        if (entry.toString().endsWith(".xls") && !entry.toString().contains(OUTPUTFILENAME)) {
            resultXls.add(entry);
        }
        if ( entry.toString().contains(OUTPUTFILENAME) ) {
            System.out.println("");
            System.out.println("!!!Обнаружен файл "+entry.toString()+". Обработка файла с этим именем запрещена. Для включения его в обработку измените его название.");
            String fileSeparator=System.getProperty(FILESEPARATOR);
            //и если кроме названия папки еще и путь равен пути программы, то редупреждаем о перезаписи
            if ( entry.toString().substring(0, entry.toString().lastIndexOf(fileSeparator)).equals(System.getProperty(CURRENTDIRECTORY)) ) {
                System.out.println("!!!!!!!!!!!!!Файл "+entry.toString()+" записан в папку с самой программой. Он будет перезаписан!");
                System.out.println("");
            }
        }
    }

}
